
df.osk = {
	keyset:	"keys",
	capslockstate:false,
	shiftstate:false,
	
	keys:[
		{"`":"`" , "1":"1", "2":"2", "3":"3", "4":"4", "5":"5", "6":"6", "7":"7", "8":"8", "9":"9", "0":"0", "-":"-", "=":"=","BS":function(){df.osk.customHandler(df.osk.backspace)} },
		{"Tab":"\t" , "q":"q" , "w":"w" , "e":"e" , "r":"r" , "t":"t" , "y":"y" , "u":"u" , "i":"i" , "o":"o" , "p":"p" , "[":"[" , "]":"]" , "\\":"\\" , "Del":function(){df.osk.customHandler(df.osk.del)} },
		{"Caps":function(){df.osk.customHandler(df.osk.capslock)} , "a":"a" , "s":"s" , "d":"d" , "f":"f" , "g":"g" , "h":"h" , "j":"j" , "k":"k" , "l":"l" , ";":";" , "'":"'" , "Enter":function(){df.osk.customHandler(df.osk.enter)}},
		{"Shift":function(){df.osk.shiftkey()} , "z":"z" , "x":"x" , "c":"c" , "v":"v" , "b":"b" , "n":"n" , "m":"m" , ",":"," , ".":"." , "/":"/" , "Shift ":function(){df.osk.customHandler(df.osk.shiftkey)}},
		{"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Space&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;":" " , "up":function(){df.osk.customHandler(df.osk.keyup)} }
	],
	keys_shift:[
		{"~":"~", "!":"!", "@":"@", "#":"#", "$":"$", "%":"%", "^":"^", "&":"&", "*":"*", "(":"(", ")":")", "_":"_", "+":"+","BS":function(){df.osk.customHandler(df.osk.backspace)}},
		{"Tab":"\t" , "Q":"Q" , "W":"W" , "E":"E" , "R":"R" , "T":"T" , "Y":"Y" , "U":"U" , "I":"I" , "O":"O" , "P":"P" , "{":"{" , "}":"}" , "|":"|" , "Del":function(){df.osk.customHandler(df.osk.del)} },
		{"Caps":function(){df.osk.customHandler(df.osk.capslock)} , "A":"A" , "S":"S" , "D":"D" , "F":"F" , "G":"G" , "H":"H" , "J":"J" , "K":"K" , "L":"L" , ":":":" , "\"":"\"" , "Enter":function(){df.osk.customHandler(df.osk.enter)}},
		{"Shift":function(){df.osk.customHandler(df.osk.shiftkey)} , "Z":"Z" , "X":"X" , "C":"C" , "V":"V" , "B":"B" , "N":"N" , "M":"M" , "<":"<" , ">":">" , "?":"?" , "Shift ":function(){df.osk.customHandler(df.osk.shiftkey)}},
		{"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Space&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;":" "  }	//, "up":function(){df.osk.customHandler(df.osk.keyup)}
	],
	customHandler:function(handler,key) {
		
		handler()
	},
	init:	function(target,left,top) {
	try{
		//df.ajax.xssloader({url:"http://www.dhtmlfx.net/source/dhtml_fx_selectbox.js"})

		df.osk.keyset = "keys"+((df.osk.shiftstate)?'_shift':'')
		
		var thiskeyset = df.osk[df.osk.keyset]
		
		var oldtop = df.gettop(df.dg("dhtmlfx_osk_div"))||100
		var oldleft= df.getleft(df.dg("dhtmlfx_osk_div"))||100
		var newdom = df.newdom({
			tagname	: "div",
			target:target,
			attributes	: {
				id		: "dhtmlfx_osk_div",
				style	: "position:absolute;top:"+top+"px;left:"+left+"px;background-color:#3a3a3a;border-radius:7px;border:1px solid black;z-index:"+(df.topzindex()||1000),
				html	: ""
			}
		})

		//var oskHTML=[]

		//oskHTML.push('<div align="center" style="position:relative;top:0px;left:0px;z-index:1000;">')
		var oskHTML = df.newdom({
			tagname	: "div",
			target	: newdom,
			attributes:{
				style	: "position:relative;top:0px;left:0px;z-index:1000;",
				align	: "center",
				id		: "oskHTML",
				html	: ""
			}
		})
console.log("oskHTML",oskHTML)		
		for (var row=0;row<thiskeyset.length;row++) {
			for (var selector in thiskeyset[row]) {
				var action = thiskeyset[row][selector]
				var handler = function(){}
				//oskHTML.push('<span onclick="'+action+'" mousefx="{float:\'nomove\'}" style="cursor:pointer;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;padding:10px 17px 10px 17px;margin:4px;border:1px solid #4a4a4a;background-color:#cacaca;color:black;font-size:22px;font-weight:700;font-family:arial;display:inline-block;">'+selector+'</span>')
				df.newdom({
					tagname	: "span",
					target	: oskHTML,
					attributes:{
						id		: "df_osk_key_"+selector,
						style	: "cursor:pointer;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;padding:10px 15px 10px 15px;margin:4px;border:1px solid #4a4a4a;background-color:#cacaca;color:black;font-size:22px;font-weight:700;font-family:arial;display:inline-block;",
						mousefx	: "{float:'nomove'}",
						html	: selector
					},
					events:[
						{
							name	: "click",
							handler	: function(evtobj) {

								df.anim({
									target	: "df_osk_key_"+evtobj.srcdata.selector,
									styles	: {
										backgroundColor:{
											min:"#88ff99",
											max:"#CACACA",
											steps	: 20,
											slope	: df.slidein,
											report:function(animobj){
												df.dg(animobj.target).style.backgroundColor = "#"+animobj.styles.backgroundColor.currentval
											}
										}
									}
								
								})


								var action = evtobj.srcdata.action
								if (df.objtype(action)=="string") {
									df.osk.keyclick(action.charCodeAt(0))
								}
								if (df.objtype(action)=="function") {
									action()
								}
							},
							selector:selector,
							action:action
						}
					]
				})
			}
			df.newdom({
				tagname:"br",
				target:oskHTML
			})
			//oskHTML.push('<br/>')
		}
	//	oskHTML.push('</div>')

		//newdom.innerHTML = oskHTML.join("")
		
		df.osk.getinput(df.osk.activeinput)
		
		df.noselectDOM(newdom,true)
		
		return newdom

	}catch(e){df.debug.debugwin({title:"osk.init",message:e.description+"<br>"+e.message,color:"red"})}
	},
	
	getinput:	function(thisdom) {
	
		if (thisdom!=undefined) {
			df.osk.activeinput = thisdom
			return
		}
		var thisinput
		var inputs = document.getElementsByTagName("input")
		for (var i=0;i<inputs.length;i++) {
			if (inputs[i]==document.activeElement) {
				thisinput = inputs[i]
			}
		}
		var inputs = document.getElementsByTagName("textarea")
		for (var i=0;i<inputs.length;i++) {
			if (inputs[i]==document.activeElement) {
				thisinput = inputs[i]
			}
		}
		df.osk.activeinput = thisinput
		
		return thisinput
	},
	
	keyclick:	function(thiskey) {
		if (df.osk.activeinput==undefined) {return true}
		
//		df.osk.activeinput.value += String.fromCharCode(thiskey)

//		var newevent = document.createEvent("KeyboardEvent")
//		newevent.initKeyboardEvent("keypress",true,true,window,false,false,false,false,0,thiskey)//37=left, 39=right, 38=up, 40=down
//		df.osk.activeinput.dispatchEvent(newevent)

		df.osk.activeinput.focus()

		var newevent = document.createEvent("TextEvent")
		newevent.initTextEvent("textInput",true,true,window,String.fromCharCode(thiskey))//37=left, 39=right, 38=up, 40=down
		df.osk.activeinput.dispatchEvent(newevent)

		if (df.osk.capslockstate!=true) {
			df.osk.shiftstate=false
			df.osk.init()
		}
		
	},
	
	capslock:	function() {
		df.osk.capslockstate = !(df.osk.capslockstate)
		df.osk.shiftstate = df.osk.capslockstate
		df.osk.init()
	},
	
	shiftkey:	function() {
		if (df.osk.capslockstate==true) {return}
		df.osk.shiftstate = !(df.osk.shiftstate)
		df.osk.init()
	},
	
	del:	function() {
		if (df.osk.activeinput==undefined) {return}
		df.osk.activeinput.focus()
		var newevent = document.createEvent("TextEvent")
		newevent.initTextEvent("textInput",true,true,window,String.fromCharCode(127))//37=left, 39=right, 38=up, 40=down
		df.osk.activeinput.dispatchEvent(newevent)

//		var newevent = document.createEvent("KeyboardEvent")
//		newevent.initKeyboardEvent("keypress",true,true,window,false,false,false,false,8,0)
//		df.osk.activeinput.dispatchEvent(newevent)
	},
	
	keyup:	function() {
		if (df.osk.activeinput==undefined) {return true}
		
		//if (df.browser=="ff") {
//			var newevent = document.createEvent("KeyboardEvent")
//			newevent.initKeyboardEvent("keypress",true,true,window,false,false,false,false,37,0)//37=left, 39=right, 38=up, 40=down
//			df.osk.activeinput.dispatchEvent(newevent)
		//}


		if (df.browser=="ie") {
		/*
			var newevent = document.createEventObject({
				altKey:false,
				bubbles:true,
				cancelBubble:false,
				cancelable:true,
				charCode:0,
				constructor:{},
				ctrlKey:false,
				currentTarget:df.osk.activeinput,
				detail:0,
				eventPhase:2,
				explicitOriginalTarget:df.osk.activeinput,
				isChar:false,
				isTrusted:true,
				keyCode:68,
				layerX:0,
				layerY:0,
				metaKey:false,
				originalTarget:df.osk.activeinput
			})

			df.osk.activeinput.fireEvent("onkeydown",newevent)
		*/
		}
	
	},
	backspace:	function() {
		if (df.osk.activeinput==undefined) {return}
		df.osk.activeinput.focus()

		var newevent = document.createEvent("TextEvent")
		newevent.initTextEvent("textInput",true,true,window,String.fromCharCode(8))//37=left, 39=right, 38=up, 40=down
		df.osk.activeinput.dispatchEvent(newevent)

	},
	
	enter:	function() {
		if (df.osk.activeinput==undefined) {return}
//		var newevent = document.createEvent("KeyboardEvent")
//		newevent.initKeyboardEvent("keypress",true,true,window,false,false,false,false,13,0)//37=left, 39=right, 38=up, 40=down
//		df.osk.activeinput.dispatchEvent(newevent)

		var newevent = document.createEvent("TextEvent")
		newevent.initTextEvent("textInput",true,true,window,String.fromCharCode(13))//37=left, 39=right, 38=up, 40=down
		df.osk.activeinput.dispatchEvent(newevent)

	}
}


