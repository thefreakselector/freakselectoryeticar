
gulp.task('/?', function(resolve){

  runSequence(["gulptasks","getargv"],resolve)

})


gulp.task("gulptasks", function(resolve){
/*
Show all Gulp tasks with descriptions
*/

  console.log()
  console.log("*---------------------------------------------------------------------------------------*")
  console.log("*                                                                                       *")
  console.log("*  Descriptions of Gulp tasks are taken from the first comment block inside a function. *")
  console.log("*                                                                                       *")
  console.log("*---------------------------------------------------------------------------------------*")
  console.log()

  var descriptions = {};

  var tasks = [];
  var available_tasks = {};

  var comment;

  var max_length = 0;
  for (task_name in gulp.tasks){
    max_length = Math.max(max_length,task_name.length)
  }
  max_length = Math.round(max_length);

  for (task_name in gulp.tasks){

    available_tasks[task_name] = true;

    var fn_str = gulp.tasks[task_name].fn.toString().split("{")[1];
    var regexp = /\/\*(.|\r\n)*?\*\//m
    var comment = (fn_str.match(regexp)||[])[0];

    if (comment){
      comment = comment.substring(2,comment.length-2).replace(/^\s{1,}/g,'');
      comment_lines = comment.split("\r\n")
      for (var i = 0;i < comment_lines.length;i++){
        if (i==0){continue}
        comment_lines[i] = (" ".repeat(max_length+1)) + comment_lines[i];
      }
      comment = comment_lines.join("\r\n")
    }

    tasks.push({
      task        : gulp.tasks[task_name],
      task_name   : task_name,
      description : comment
    })
  }

  tasks.sort(function(a,b){
    return a.task_name < b.task_name ? -1 : a.task_name > b.task_name ? 1 : 0;
  })


  for (var i = 0;i < tasks.length;i++){
    var current_task = descriptions[tasks[i].task_name];
    descriptions[tasks[i].task_name] = current_task || {
      task_name   : tasks[i].task_name,
      description : tasks[i].description,
      created     : Date.now()
    }
    descriptions[tasks[i].task_name].description = tasks[i].description
  }

  for (var item in descriptions){
    var item      = descriptions[item];
    var tabs_len  = (max_length - Math.floor(item.task_name.length));
    var tabs      = " ".repeat( tabs_len+1 );

    console.log( cc.set( item.description ? "fg_cyan" : "fg_yellow" , item.task_name) + tabs  + (item.description || cc.set("fg_red", "TODO: [ write description ]")) )
    console.log()

  }

  console.log("\n");


})

gulp.task("getargv",function(){
/*
  Show all gulp input arguments (with comments).
*/
  getFilesRecursiveAsync(root_folder+path.sep+"Gulp", function(err,files){

    files.push(root_folder+path.sep+"gulpfile.js");
    
    var argvs = {};

    for (var i=0;i<files.length;i++){
      var file = fs.readFileSync(files[i],'utf8');
      var this_argv = file.match(/argv\.(.*?)[^a-zA-Z0-9]/igm);


      if (this_argv!==null){
        for (var ix=0;ix<this_argv.length;ix++){
          var arg = this_argv[ix].split(".")[1].replace(/[^a-zA-Z0-9]/g,"");
          if (arg === "length"){continue}

          //----------------------------------------------------------------------------------------
          //get first comment block after argv definition

          argvs[arg] = {}

          var commentreg = new RegExp("argv\\."+arg+".*?[\\r\\n]{0,}.*?\\*(.|[\\r\\n])*?\\*\\/","igm");

          var this_argv_comment = file.match(commentreg)

          if (this_argv_comment){
            this_argv_comment = this_argv_comment[0].match(/\/\*(.|[\r\n])*?\*\//);
            if (this_argv_comment){
              argvs[arg].comment = this_argv_comment[0].replace(/\/\*|\*\//g,"")
              argvs[arg].comment = cc.set(argvs[arg].comment ? "fg_lightgray" : "fg_red", argvs[arg].comment||"")//"           "+((argvs[arg].comment||"").split("\n").join("           \n")) )
            }
          }
          //----------------------------------------------------------------------------------------

        }
      }
    }
    console.log("\n\nargument inputs: ")

    console.log()
    console.log("*------------------------------------------------------------------------------------========================---*")
    console.log("*                                                                                                               *")
    console.log("*  Descriptions of Gulp input arguments are taken from the first comment block after the first argv definition. *")
    console.log("*                                                                                                               *")
    console.log("*---------------------------------------------------------------------------------------------------------------*")
    console.log()

    for (var arg in argvs){
      console.log(" ", cc.set(argvs[arg].comment ? "fg_cyan" : "fg_yellow", "--"+arg),"\n", argvs[arg].comment || "none.","\n\n")
    }

  }, "js^");


})
