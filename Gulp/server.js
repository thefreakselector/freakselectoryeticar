//----------------------------------------------------------------------------------
var CWD = process.cwd();
var qs = require('querystring');

var execFile = require('child_process').execFile;

var freakselectorIsRunning = false;
var start_browser_tmr = undefined;
//----------------------------------------------------------------------------------

gulp.task("start_static_server", function(){

	startLiveReload()

	var serve = gulpServe({
		root:['/'],
		port:8080,			//default port is 8881
		hostname: "0.0.0.0",
		middleware: function(req,res){

			var this_url = req.url.split("?")[0];
			var url_split = this_url.split("/").slice(1);			

			if (url_split[0] && Handlers[url_split[0]]){
				//Not a static file, these are method handlers.
				console.log(cc.set("fg_green",url_split[0]+" : "+this_url))						
				Handlers[url_split[0]](req,res)

			} else {
				//This is for static files

				var this_url = decodeURIComponent(req.url.split("?")[0]);

				if (this_url === "/"){
					this_url = "/index.html"
				}

				if (this_url.indexOf('/quitFreakSelector')!==-1){
					process.abort()
				}

				var url_split = this_url.split("/").slice(1);		

				var static_file = path.resolve(root_folder+this_url)

				fs.stat(static_file, function(err,stat){
					if (err){
						console.log(cc.set("fg_yellow","File not found : "+this_url))		
						res.setHeader("Content-Type", "plain/text");
						res.statusCode = 404;
						res.end("File Not Found.")
						return
					}

					console.log(cc.set("fg_cyan","Static file : "+this_url))		

					var obj = {}
					obj.stat = stat
					obj.file_path = static_file;
					obj.file_name = url_split.slice(-1)[0];
					getBinaryFile(req,res,obj)

				})

			}

		}

	})

	if (args.serve){
		runSequence("openFreakSelectorWebInterface", "watch")
	}

	return serve()

})

//----------------------------------------------------------------------------------

gulp.task("watch",function(){
  var base_folder = path.resolve(root_folder+'/**');

  gulpWatch(base_folder, function(evt){
console.log(cc.set('fg_red','>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'))
  	//triggerLiveReload()
  })
})
//----------------------------------------------------------------------------------

function getPostData(req, callback){
	console.log("req.method",req.method)
	if (req.method.toLowerCase() === 'post' ) {
	  var body = '';
	  req.on('data', function(chunk) {
	    body += chunk;
	  });
	  req.on('end', function() {
	    var data = qs.parse(body);
	    // now you can access `data.email` and `data.password`
	    //req.body = JSON.stringify(data);
	    callback(data)
	  });
	} else {
		console.log(cc.set("fg_red","getPostData is using method "+req.method+" but needs to be POST"))
		callback()
	}
}

//----------------------------------------------------------------------------------

global.open = require("gulp-open");

gulp.task("openFreakSelectorWebInterface",function(){

	setTimeout(function(){
		
		console.log("global.livereload_browser_connected",global.livereload_browser_connected)

		if (global.livereload_browser_connected){ return }

	  /*gulp.src(__filename)
	  	.pipe(open({uri: 'http://localhost:8080/', app:'chrome'}));
*/
		runexe()
		console.log("openFreakSelectorWebInterface")


	},50)



})

//----------------------------------------------------------------------------------

var Handlers = {
	"getDrives"								: getDrives,
	"getFreakselectorPlugins"	: getFreakselectorPlugins,
	"getMilkdropPresets"			: getMilkdropPresets,
	"getFolders"							: getFolders,
	"setFolders"							: setFolders,
	"getAudio"								: getAudio,
	"saveFolderPaths"					: saveFolderPaths,
	"getFolderPaths"					: getFolderPaths,
	"saveRecentlyPlayed"			: saveRecentlyPlayed,
	"getRecentlyPlayed"				: getRecentlyPlayed,
	"nircmd"									: runNircmd,
	"saveVideoFrame"					: saveVideoFrame,
	"getPath" 								: getPath,
	"FreakSelectorRunning"	: FreakSelectorRunning
}

//----------------------------------------------------------------------------------

function FreakSelectorRunning(req,res){
	console.log("<<<FreakSelectorRunning>>>")
	freakselectorIsRunning = true;
	if (start_browser_tmr){clearTimeout(start_browser_tmr);start_browser_tmr = undefined}
	res.setHeader("Content-Type", "plain/text");
	res.statusCode = 200;
	res.end("okay")
}


function runNircmd(req,res){
	var this_url = req.url.split("?")[0];
	var url_split = this_url.split("/").slice(1);		

	var this_cmd = atob(url_split[1]);

	exec(CWD + "\\nircmd\\nircmdc.exe "+this_cmd, function(err,stdout,stderr){
console.log(err,'\n------------------------------\n',stdout,'\n------------------------------\n',stderr)		
		res.end()
	})

}

//----------------------------------------------------------------------------------

function saveRecentlyPlayed(req,res){

	var this_url = req.url.split("?")[0];
	var url_split = this_url.split("/").slice(1);		

	var body = [];

	req.on('data', function(chunk) {

	  body.push(chunk);

	}).on('end', function() {

	  body = Buffer.concat(body).toString();


	  var recently_played = JSON.parse(body);

	  var recently_played_file;
		try{
			recently_played_file = require(path.resolve(root_folder + path.sep + "recently_played.json")) || {};
		}catch(e){
			recently_played_file = {};
		}

		var media_type = decodeURIComponent(url_split[1]);

		recently_played_file[media_type] = recently_played;

		fs.writeFile(root_folder + path.sep + 'recently_played.json', JSON.stringify(recently_played_file), function (err,data) {
			if (err) {
				res.setHeader("Content-Type", "text/plain");
				res.statusCode = 500;
				res.end("Error writing folder_paths.json file to disk")
				return
			}
			res.setHeader("Content-Type", "text/plain");
			res.statusCode = 200;
			res.end("saved")
		})

	});

}

//----------------------------------------------------------------------------------

var saveVideo_Folder = "D:\\SavedVideo\\";

var savedFolders = {};

function saveVideoFrame(req,res){

	var this_url = req.url.split("?")[0];

	var url_split = this_url.split("/").slice(1);		

	var body = [];

	req.on('data', function(chunk) {

	  body.push(chunk);

	}).on('end', function() {

	  body = Buffer.concat(body).toString();
//console.log("saveVideoFrame: ",body)

		var data = atob(body).replace(/^data:image\/\w+;base64,/, "");

//console.log("datA",data)

		var datenow = new Date();
		var date_folder = ("0"+(datenow.getMonth()+1)).substr(-2) + (datenow.getDate()) + datenow.getYear().toString().substr(-2);
		if (!savedFolders[date_folder]){
			var folder_exists
			try{folder_exists = fs.statSync(saveVideo_Folder + date_folder)}catch(e){}
			if (!folder_exists){
				fs.mkdirSync(saveVideo_Folder + date_folder)
				savedFolders[date_folder] = true
			} else {
				savedFolders[date_folder] = true
			}
		}

		var buf = new Buffer(data, 'base64');

		var frame_filename = Date.now()+".png"		

		fs.writeFile(saveVideo_Folder + date_folder + path.sep + frame_filename, buf, function (err,data) {			if (err) {
				res.setHeader("Content-Type", "text/plain");
				res.statusCode = 500;
				res.end("Error writing video capture file to disk")
				return
			}
			res.setHeader("Content-Type", "text/plain");
			res.statusCode = 200;
			res.end("saved")
		})

	});

}

//----------------------------------------------------------------------------------

function getRecentlyPlayed(req,res){

	var this_url = req.url.split("?")[0];
	var url_split = this_url.split("/").slice(1);		

	var media_type = decodeURIComponent(url_split[1]);

  var recently_played_file;
	try{
		recently_played_file = require(path.resolve(root_folder + path.sep + "recently_played.json")) || {};
	}catch(e){
		recently_played_file = {};
	}

	res.setHeader("Content-Type", "application/json");
	res.statusCode = 200;
	res.end(JSON.stringify(recently_played_file[media_type] || {} ))

}

//----------------------------------------------------------------------------------

function saveFolderPaths(req,res){
	
	var new_folder_paths = JSON.parse(req.body.folder_paths);

	resolveFolderPaths(new_folder_paths)

	global.folder_paths = new_folder_paths;
console.log("folder_paths",global.folder_paths )	
/*
	fs.writeFile(root_folder + path.sep + 'folder_paths.json', JSON.stringify(folder_paths), function (err,data) {
	  if (err) {
			res.setHeader("Content-Type", "text/plain");
			res.statusCode = 500;
			res.end("Error writing folder_paths.json file to disk")
			return
	  }
*/
	if (res){
			res.setHeader("Content-Type", "application/json");
			res.statusCode = 200;
			res.end(JSON.stringify(global.folder_paths ))
	}
/*
	});

*/
}

//----------------------------------------------------------------------------------

function setFolders(req,res){

	var this_url = req.url.split("?")[0];
	var url_split = this_url.split("/").slice(1);		
	var media_type = decodeURIComponent(url_split[1]);

	getPostData(req, function(data){

		global.folder_paths[media_type] = JSON.parse(data.mapped_folders)
console.log("FOLDER PATHS",JSON.stringify(global.folder_paths,null,4))

		resolveFolderPaths(global.folder_paths)


		fs.writeFile(root_folder + path.sep + 'folder_paths.json', JSON.stringify(global.folder_paths), function (err,data) {
		  if (err) {
				res.setHeader("Content-Type", "text/plain");
				res.statusCode = 500;
				res.end("Error writing folder_paths.json file to disk")
				return
		  }

			res.setHeader("Content-Type", "application/json");
			res.statusCode = 200;
			res.end(JSON.stringify( data ))

		});


	})


}

//----------------------------------------------------------------------------------

try{
	global.folder_paths = require(path.resolve(root_folder + path.sep + "folder_paths.json"));
}catch(e){
	global.folder_paths = {};

	var folder_paths_exists
	try{folder_paths_exists = fs.statSync(path.resolve(root_folder + path.sep + "folder_paths.json"))}catch(e){}
	if (!folder_paths_exists){
		fs.writeFileSync(root_folder + path.sep + 'folder_paths.json', JSON.stringify(folder_paths), "UTF8");
	}
}

function resolveFolderPaths(this_folder_paths){
	for (var plugin in this_folder_paths){
		for (var plugin_folder in global.folder_paths [plugin]){
			this_folder_paths[plugin][plugin_folder] = path.resolve(this_folder_paths[plugin][plugin_folder]);
		}
	}	
}

resolveFolderPaths(global.folder_paths );
saveFolderPaths({body:{folder_paths:JSON.stringify(global.folder_paths )}})

//----------------------------------------------------------------------------------

function getFolderPaths(req,res){

	res.setHeader("Content-Type", "application/json");
	res.statusCode = 200;
	res.end(JSON.stringify(global.folder_paths ))

}

//----------------------------------------------------------------------------------

function getFolderList(req,res){

	var folders = [];

	for (var folder in global.folder_paths ){
		folders.push(folder)
	}

	res.setHeader("Content-Type", "application/json");
	res.statusCode = 200;
	res.end(JSON.stringify(folders))

}

//----------------------------------------------------------------------------------

function getFolders(req,res){

	var this_url = req.url.split("?")[0];
	var url_split = this_url.split("/").slice(1);		

	var this_plugin = unescape(url_split[1]);
	var this_folder = unescape(url_split[2]);

	if (url_split[2] === undefined) {
		var base_folders = [];
		for (var folder in global.folder_paths[this_plugin]){
			base_folders.push(folder)
		}

		res.setHeader("Content-Type", "application/json");
		res.statusCode = 200;
		res.end(JSON.stringify(base_folders))
		return
	}

	var folder_base = global.folder_paths[this_plugin][this_folder];


	if (folder_base){
		getFoldersRecursiveAsync(folder_base,function(err,data){
			if (err){
				console.log(err)
				res.setHeader("Content-Type", "application/text");
				res.statusCode = 500;
				res.end("ERROR")
				return
			}

			for (var i=0;i<data.length;i++){
				data[i] = this_folder + data[i].substr(folder_base.length).replace(/\\/g,"/")
			}

			res.setHeader("Content-Type", "application/json");
			res.statusCode = 200;
			res.end(JSON.stringify(data))

		})

	} else {
		console.log("No folder defined for:",url_split[1])
		res.setHeader("Content-Type", "text/plain");
		res.statusCode = 200;
		res.end("No Folder Found")
	}
}

//----------------------------------------------------------------------------------

function getPath(req,res){

	var this_url = req.url.split("?")[0];

	var url_split = this_url.split("/").slice(1);		

	var this_folder = decodeURIComponent(url_split[1]);
console.log("GET PATH:",this_folder)
	getFoldersRecursiveAsync(this_folder, function(err,data){
console.log(err,data)
			if (!data){ data = {folders:[]}}

		res.setHeader("Content-Type", "application/json");
		res.statusCode = 200;
		res.end(JSON.stringify(data.folders))

	},undefined,true)
}

//----------------------------------------------------------------------------------

function getAudio(req,res){

	var obj = {};

	var this_url = decodeURIComponent(req.url).split("?")[0];
	var url_split = this_url.split("/").slice(1);		

	var this_map    = url_split[1];
	var this_folder = url_split[2];
	var folder_path = global.folder_paths[this_map][this_folder];

	obj.file_path = folder_path + path.sep + (url_split.slice(3).join(path.sep) );

	obj.file_name = url_split.slice(-1)[0]

	getBinaryFile(req, res, obj)

}

//----------------------------------------------------------------------------------

function getBinaryFile(req, res, obj){

	if (obj.file_path){

		if (!obj.stat){
			fs.stat(obj.file_path, sendBinaryFile);
		} else {
			sendBinaryFile(null, obj.stat)
		}

 		function sendBinaryFile(err, stat){

			var readStream
 			try{
		    readStream = fs.createReadStream(obj.file_path);
		  }catch(e){
				res.setHeader("Content-Type", "application/text");
				res.statusCode = 500;
				res.end("ERROR: File read error: "+e.message)
		  	return
		  }

	    // We replaced all the event handlers with a simple call to readStream.pipe()
	    readStream.on('open', function () {
	    // This just pipes the read stream to the response object (which goes to the client)

				res.writeHead(200, {
				    'Content-Type': getMimeType(obj.file_path),
				    'Content-Length': stat.size,
				    //'Content-Disposition': 'attachment; filename='+encodeURIComponent(obj.file_name)
				});    
		    readStream.pipe(res);
	    });

	    readStream.on('error', function(err) {
					res.statusCode = 500;
	        res.end(err);
			});
		}

	}	else {
		console.log("No folder defined for:",url_split[1])
		res.setHeader("Content-Type", "application/text");
		res.statusCode = 500;
		res.end("ERROR")
	}

}

//----------------------------------------------------------------------------------
function getImage(req,res){
}
//----------------------------------------------------------------------------------


function getDrives(req,res){
/*
	var drivelist = require("drivelist");

	drivelist.list(function(err,disks){
		if (err){
			res.statusCode = 200;
			res.end(err)
			return 
		}
		console.log(JSON.stringify(disks))
		res.setHeader("Content-Type", "application/json");
		res.statusCode = 200;
		res.end(JSON.stringify(disks))

	})
*/

//diskinfo does not work on osx :(

var diskInfo = require('diskinfo');

	diskInfo.getDrives(function(err,data){
		if (err){
			console.log("error getting drives:",err)
			res.setHeader("Content-Type", "application/text");
			res.statusCode = 500;
			res.end(JSON.stringify({"error":true,err:err}))
			return
		}

		var drives = {};
		for (var i=0;i<data.length;i++){
			drives[data[i].mounted] = drives[data[i].mounted] || {
				description:"",
				mountpoint: data[i].mounted,
				name: data[i].name
			}
		}

		var drive_arr = []
		for (var drive in drives){
			drive_arr.push(drives[drive]);
		}

		res.setHeader("Content-Type", "application/json");
		res.statusCode = 200;
		res.end(JSON.stringify(drive_arr))
	})

}


gulp.task("diskinfo",function(){
	var drivelist = require("drivelist");

	drivelist.list(function(err,disks){
		console.log(disks)
	})


})
//----------------------------------------------------------------------------------

function getFreakselectorPlugins(req,res){
	
	var this_url = decodeURIComponent(req.url).split("?")[0];
	var url_split = this_url.split("/").slice(1);		
	
	var folder_base = path.resolve(root_folder+'/freakselector_plugins')

	getFilesRecursiveAsync(folder_base, function(err,data){

		for (var i=0;i<data.length;i++){
			data[i] = '/freakselector_plugins' + data[i].substr(folder_base.length).replace(/\\/g,"/")
		}

		var obj = {
			folders:[],
			files:data
		}

		res.setHeader("Content-Type", "application/json");
		res.statusCode = 200;
		res.end(JSON.stringify(obj))

	}, ".js$", undefined, true)

}

//--------------------------------------------------------------

function getMilkdropPresets(req,res){

}

//--------------------------------------------------------------
//S3 Mime Types

global.mime_types = {
  'a'      : 'application/octet-stream',
  'ai'     : 'application/postscript',
  'aif'    : 'audio/x-aiff',
  'aifc'   : 'audio/x-aiff',
  'aiff'   : 'audio/x-aiff',
  'au'     : 'audio/basic',
  'avi'    : 'video/x-msvideo',
  'bat'    : 'text/plain',
  'bin'    : 'application/octet-stream',
  'bmp'    : 'image/x-ms-bmp',
  'c'      : 'text/plain',
  'cdf'    : 'application/x-cdf',
  'csh'    : 'application/x-csh',
  'css'    : 'text/css',
  'dll'    : 'application/octet-stream',
  'doc'    : 'application/msword',
  'dot'    : 'application/msword',
  'dvi'    : 'application/x-dvi',
  'eml'    : 'message/rfc822',
  'eps'    : 'application/postscript',
  'etx'    : 'text/x-setext',
  'exe'    : 'application/octet-stream',
  'gif'    : 'image/gif',
  'gtar'   : 'application/x-gtar',
  'h'      : 'text/plain',
  'hdf'    : 'application/x-hdf',
  'htm'    : 'text/html',
  'html'   : 'text/html',
  "ico"    : "image/ico",
  'jpe'    : 'image/jpeg',
  'jpeg'   : 'image/jpeg',
  'jpg'    : 'image/jpeg',
  'js'     : 'application/x-javascript',
  'json'	 : 'text/json',
  'ksh'    : 'text/plain',
  'latex'  : 'application/x-latex',
  'm1v'    : 'video/mpeg',
  'man'    : 'application/x-troff-man',
  'me'     : 'application/x-troff-me',
  'mht'    : 'message/rfc822',
  'mhtml'  : 'message/rfc822',
  'mif'    : 'application/x-mif',
  'mov'    : 'video/quicktime',
  'movie'  : 'video/x-sgi-movie',
  'mp2'    : 'audio/mpeg',
  'mp3'    : 'audio/mpeg',
  'mp4'    : 'video/mp4',
  'mpa'    : 'video/mpeg',
  'mpe'    : 'video/mpeg',
  'mpeg'   : 'video/mpeg',
  'mpg'    : 'video/mpeg',
  'ms'     : 'application/x-troff-ms',
  'nc'     : 'application/x-netcdf',
  'nws'    : 'message/rfc822',
  'o'      : 'application/octet-stream',
  'obj'    : 'application/octet-stream',
  'oda'    : 'application/oda',
  'ogv'		 : 'video/ogg',
  'pbm'    : 'image/x-portable-bitmap',
  'pdf'    : 'application/pdf',
  'pfx'    : 'application/x-pkcs12',
  'pgm'    : 'image/x-portable-graymap',
  'png'    : 'image/png',
  'pnm'    : 'image/x-portable-anymap',
  'pot'    : 'application/vnd.ms-powerpoint',
  'ppa'    : 'application/vnd.ms-powerpoint',
  'ppm'    : 'image/x-portable-pixmap',
  'pps'    : 'application/vnd.ms-powerpoint',
  'ppt'    : 'application/vnd.ms-powerpoint',
  'pptx'    : 'application/vnd.ms-powerpoint',
  'ps'     : 'application/postscript',
  'pwz'    : 'application/vnd.ms-powerpoint',
  'py'     : 'text/x-python',
  'pyc'    : 'application/x-python-code',
  'pyo'    : 'application/x-python-code',
  'qt'     : 'video/quicktime',
  'ra'     : 'audio/x-pn-realaudio',
  'ram'    : 'application/x-pn-realaudio',
  'ras'    : 'image/x-cmu-raster',
  'rdf'    : 'application/xml',
  'rgb'    : 'image/x-rgb',
  'roff'   : 'application/x-troff',
  'rtx'    : 'text/richtext',
  'sgm'    : 'text/x-sgml',
  'sgml'   : 'text/x-sgml',
  'sh'     : 'application/x-sh',
  'shar'   : 'application/x-shar',
  'snd'    : 'audio/basic',
  'so'     : 'application/octet-stream',
  'src'    : 'application/x-wais-source',
  'swf'    : 'application/x-shockwave-flash',
  't'      : 'application/x-troff',
  'tar'    : 'application/x-tar',
  'tcl'    : 'application/x-tcl',
  'tex'    : 'application/x-tex',
  'texi'   : 'application/x-texinfo',
  'texinfo': 'application/x-texinfo',
  'tif'    : 'image/tiff',
  'tiff'   : 'image/tiff',
  'tr'     : 'application/x-troff',
  'tsv'    : 'text/tab-separated-values',
  'txt'    : 'text/plain',
  'ustar'  : 'application/x-ustar',
  'vcf'    : 'text/x-vcard',
  'wav'    : 'audio/x-wav',
  "woff"  : "application/font-woff",
  "woff2" : "application/font-woff",
  'wiz'    : 'application/msword',
  'wsdl'   : 'application/xml',
  'xbm'    : 'image/x-xbitmap',
  'xlb'    : 'application/vnd.ms-excel',
  'xls'    : 'application/vnd.ms-excel',
  'xlsx'    : 'application/vnd.ms-excel',
  'xml'    : 'text/xml',
  'xpdl'   : 'application/xml',
  'xpm'    : 'image/x-xpixmap',
  'xsl'    : 'application/xml',
  'xwd'    : 'image/x-xwindowdump',
  'zip'    : 'application/zip',

  "ttf"   : "application/font-ttf",
  "eot"   : "application/vnd.ms-fontobject",
  "otf"   : "application/font-otf",
  "svg"   : "image/svg+xml"

}


global.getMimeType = function(file){
  var filetype = file.toLowerCase().match(/\.(.*?)$/)

  if (filetype !== null){
    var mime_lookup = mime_types[ filetype[1] ];
    if (!mime_lookup){
      console.warn('No MIME Type for: ',file, '  using text/plain')
    }
    return mime_lookup || 'text/plain';
  }
  console.warn('No mine type for: ',file, '  using text/plain')
  return "text/plain"

}

//----------------------------------------------------------------------------------

function runexe(){   
	if (freakselectorIsRunning){return}
	console.log("Trying to start web browser, please wait..")
   	execFile('C:\\Users\\puma\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe',
	   	[
	   		'--disable-infobars',
	   		'--allow-outdated-plugins',
	   		'--disable-background-timer-throttling',
	   		'--start-fullscreen',
	   		'--kiosk',
	   		'--incognito',
	   		'--restore-last-session','--false',
	   		'--app-auto-launched','--true',
	   		'http://localhost:8080'
	   	], function(err, data) {  
	        if (err){
	        	console.log(err)
	        } else {
        		console.log(data.toString());    
        	}                   
    	}
    );

	if (start_browser_tmr){clearTimeout(start_browser_tmr);start_browser_tmr = undefined}
	start_browser_tmr = setTimeout(runexe, 5000)

}   

//----------------------------------------------------------------------------------
