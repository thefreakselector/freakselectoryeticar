

global.fs             = require("fs");
global.path           = require("path");
global.proc						= require("child-proc");


global.gulp           = require('gulp');

global.runSequence    = require('run-sequence');

global.gulpServe			= require('gulp-serve');

global.requireDir     = require('require-dir');

global.getFilesRecursiveAsync = require('get-files-recursive-async');
global.getFoldersRecursiveAsync = require('get-folders-recursive-async');
global.cc             = require("node-console-colors");

global.root_folder = __dirname;

global.atob						= require("atob");
global.exec						= require("child_process").exec;

//--------------------------------------------------------------------------------------
//Require all files in gulp folder

requireDir(path.resolve(root_folder + '/Gulp'), {recurse:true});

global.args = (function(){var args={};process.argv.slice(2).forEach(function(val,idx,arr){args[val.split("=")[0]]=val.split("=")[1]||true});return args})();
console.log("ARGS:\n\n",JSON.stringify(args,null,4))
//--------------------------------------------------------------------------------------
gulp.task('init',function(){
	console.log("gulp init...")
})

gulp.task('watch',["watchGulp"],function(){
	gulp.watch([path.resolve(root_folder+"/*.js"), path.resolve(root_folder+"/freakselector_plugins/*.js")],function(evt){
		console.log(evt)
	})

	gulp.watch([path.resolve(root_folder+"/folder_paths.json")], function(evt){
		console.log(evt)
		global.folder_paths = require(path.resolve(root_folder+"/folder_paths.json"));
	})
})

gulp.task('serve', ['start_static_server'], function(){

})

//--------------------------------------------------------------------------------------------
//Restart gulp if any gulp file is updated.
;(function(){

	var options = {
		'gulpfile'     : './gulpfile.js',
		'task'         : 'start_static_server',
		'pathToGulpjs' : './node_modules/gulp/bin/gulp.js'
	};


	var child_process = require('child_process');
	var gulpProcess   = null;
	var platform      = process.platform;

	gulp.task('watchGulp', ['spawn-child_process'], function() {

		getFilesRecursiveAsync(path.resolve(root_folder+"/Gulp"), function(err,gulpfiles){
		
			gulpfiles.push(path.resolve(root_folder+'/gulpfile.js'))

			gulp.watch(gulpfiles, ['kill-child_process', 'spawn-child_process']);

		})
	});
	
	//---------------------------------------------------------

	gulp.task('kill-child_process', function() {
		if(gulpProcess === null) {
			return;
		}
		console.log('kill gulp ( PID : ' + gulpProcess.pid + ' )');
		if(platform === 'win32') {
			child_process.exec('taskkill /PID ' + gulpProcess.pid + ' /T /F', function() {});
			return;
		}
		gulpProcess.kill();
	});

	//---------------------------------------------------------

	gulp.task('spawn-child_process', function() {
		var restart = '';
		//if(gulpProcess !== null) {
			restart = '--restart';
		//}
		gulpProcess = child_process.spawn('node', [options.pathToGulpjs, options.task, restart, '--gulpfile='+options.gulpfile], {stdio: 'inherit'});
		console.log('spawn gulp' + options.task + ' ( PID : ' + gulpProcess.pid + ' )');
	});

	//---------------------------------------------------------

	gulp.task("testfolders",function(){

		getFoldersRecursiveAsync("D:\\Greencloud\\Media\\Music\\", function(err,data){
console.log("getFolders:",err, JSON.stringify(data,null,2))

		}, [".mp3$"])


	})

	//---------------------------------------------------------
process.on('uncaughtException', function (exception) {
   // handle or ignore error
   console.log(exception)
});
	//---------------------------------------------------------


})();
