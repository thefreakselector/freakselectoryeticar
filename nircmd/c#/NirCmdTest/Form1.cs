/*
 * Sample for using NirCmd DLL
 * For more information about NirCmd:
 * http://www.nirsoft.net/utils/nircmd.html
 */

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;

namespace NirCmdTest
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnOpenCdRom;
		private System.Windows.Forms.Button btnCloseCdRom;
		private System.Windows.Forms.Button btnMuteSysVolume;
		private System.Windows.Forms.Button btnUnmuteSysVolume;
		private System.Windows.Forms.Button btnTurnOffMonitor;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnOpenCdRom = new System.Windows.Forms.Button();
			this.btnCloseCdRom = new System.Windows.Forms.Button();
			this.btnMuteSysVolume = new System.Windows.Forms.Button();
			this.btnUnmuteSysVolume = new System.Windows.Forms.Button();
			this.btnTurnOffMonitor = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnOpenCdRom
			// 
			this.btnOpenCdRom.Location = new System.Drawing.Point(56, 8);
			this.btnOpenCdRom.Name = "btnOpenCdRom";
			this.btnOpenCdRom.Size = new System.Drawing.Size(184, 40);
			this.btnOpenCdRom.TabIndex = 0;
			this.btnOpenCdRom.Text = "Open CD-ROM Door";
			this.btnOpenCdRom.Click += new System.EventHandler(this.btnOpenCdRom_Click);
			// 
			// btnCloseCdRom
			// 
			this.btnCloseCdRom.Location = new System.Drawing.Point(56, 56);
			this.btnCloseCdRom.Name = "btnCloseCdRom";
			this.btnCloseCdRom.Size = new System.Drawing.Size(184, 40);
			this.btnCloseCdRom.TabIndex = 1;
			this.btnCloseCdRom.Text = "Close CD-ROM Door";
			this.btnCloseCdRom.Click += new System.EventHandler(this.btnCloseCdRom_Click);
			// 
			// btnMuteSysVolume
			// 
			this.btnMuteSysVolume.Location = new System.Drawing.Point(56, 104);
			this.btnMuteSysVolume.Name = "btnMuteSysVolume";
			this.btnMuteSysVolume.Size = new System.Drawing.Size(184, 40);
			this.btnMuteSysVolume.TabIndex = 2;
			this.btnMuteSysVolume.Text = "Mute System Volume";
			this.btnMuteSysVolume.Click += new System.EventHandler(this.btnMuteSysVolume_Click);
			// 
			// btnUnmuteSysVolume
			// 
			this.btnUnmuteSysVolume.Location = new System.Drawing.Point(56, 152);
			this.btnUnmuteSysVolume.Name = "btnUnmuteSysVolume";
			this.btnUnmuteSysVolume.Size = new System.Drawing.Size(184, 40);
			this.btnUnmuteSysVolume.TabIndex = 3;
			this.btnUnmuteSysVolume.Text = "Unmute System Volume";
			this.btnUnmuteSysVolume.Click += new System.EventHandler(this.button2_Click);
			// 
			// btnTurnOffMonitor
			// 
			this.btnTurnOffMonitor.Location = new System.Drawing.Point(56, 200);
			this.btnTurnOffMonitor.Name = "btnTurnOffMonitor";
			this.btnTurnOffMonitor.Size = new System.Drawing.Size(184, 40);
			this.btnTurnOffMonitor.TabIndex = 4;
			this.btnTurnOffMonitor.Text = "Turn Off Monitor";
			this.btnTurnOffMonitor.Click += new System.EventHandler(this.btnTurnOffMonitor_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(328, 273);
			this.Controls.Add(this.btnTurnOffMonitor);
			this.Controls.Add(this.btnUnmuteSysVolume);
			this.Controls.Add(this.btnMuteSysVolume);
			this.Controls.Add(this.btnCloseCdRom);
			this.Controls.Add(this.btnOpenCdRom);
			this.Name = "Form1";
			this.Text = "NirCmd Test";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void btnOpenCdRom_Click(object sender, System.EventArgs e)
		{
			NirCmdCall.DoNirCmd("cdrom open");
		}

		private void btnCloseCdRom_Click(object sender, System.EventArgs e)
		{
			NirCmdCall.DoNirCmd("cdrom close");
		}

		private void btnMuteSysVolume_Click(object sender, System.EventArgs e)
		{
			NirCmdCall.DoNirCmd("mutesysvolume 1");
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			NirCmdCall.DoNirCmd("mutesysvolume 0");
		}

		private void btnTurnOffMonitor_Click(object sender, System.EventArgs e)
		{
			NirCmdCall.DoNirCmd("monitor off");
		}

	}


	public class NirCmdCall
	{
		[DllImport("nircmd.dll")]
		public static extern bool DoNirCmd(String NirCmdStr);
	}
}
