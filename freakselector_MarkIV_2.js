
df.debug.debugmode = true

//cross browser bullshit
window.requestAnimationFrame =
    window.requestAnimationFrame       ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame    ||
    window.oRequestAnimationFrame      ||
    window.msRequestAnimationFrame;


window.LED = {

	map:	testgrid,

	render_fast : false,
//==================================================================================

	frame:	[],

	grid_width:0,
	grid_height:0,

//==================================================================================

	start:	function(map){
			var self = this

			this.location = String(location)
			this.urlParameters={}
			var qs = this.location.split("?")[1]||""
			var ht = qs.split("#")[1]||""
			var qs_split = qs.split("&")
			var ht_split = ht.split("&")
			var params = qs_split.concat(ht_split)
			for (var i=0;i<params.length;i++) {
				var pname = params[i].split("=")[0]
				var pval  = params[i].split("=")[1]
				if (pname==undefined||pval==undefined) {continue}
				this.urlParameters[pname] = parseFloat(pval)||((pval==="true"||pval==="false")?eval(pval):String(pval).replace(/eval/g,"").replace(/function/g,""))||undefined
			}
//========================================================

			soundManager.onload = function() {
				soundManager.url = './'; // path to directory containing SoundManager2 .SWF file
				soundManager.flashVersion = 9;


				soundManager.flashLoadTimeout = 6000;
				soundManager.flash9Options.usePeakData = true;
				soundManager.flash9Options.useWaveformData = true;
				soundManager.flash9Options.useEQData = true;
				soundManager.flashPollingInterval = 22

				soundManager.waitForWindowLoad = true;
			//	soundManager.debugMode = true;
				soundManager.allowPolling = true;
				soundManager.useHighPerformance = true;
				soundManager.useFastPolling = true;
				soundManager.features.eqData = true
				soundManager.features.peakData = true
				soundManager.features.waveformData = true
				soundManager.preferFlash = true
				soundManager.html5PollingInterval  = 33

			}

//========================================================
			df.attachevent({target:window,name:"resize",handler:this.windowResize});
			this.windowResize()
//========================================================

			var width = 0
			var height= 0

			this.map = map

			var pixelcount = 0
			for (var selector in map) {
				for (var i=0;i<map[selector].length;i++) {
					if (map[selector][i].x>width) {width = map[selector][i].x}
					if (map[selector][i].y>height){height= map[selector][i].y}
					pixelcount++
				}
			}

console.log("pixelcount",pixelcount)

			width++
			height++

window.no_pixels_map=[]
for (var xx=0;xx<width;xx++){
	for (var yy=0;yy<height;yy++) {
		if (nopixel[xx]==undefined){continue}
		if (nopixel[xx][yy]!=true) {
			window.no_pixels_map.push({x:xx,y:yy})
		}
	}
}

console.log("width=",width,"\nheight=",height)


			this.grid_width = width  //* 2//|| 12
			this.grid_height= height //* 2//|| 12

			this.display_width = 352
			this.display_height= 176

			this.windowtarget = document.getElementsByTagName("body")[0]

			//-------------------------------------------

			this.LEDwindow = df.newdom({
				tagname		: "div",
				target		: this.windowtarget,
				attributes	: {
					id		: "LEDwindow",
					style	: "position:absolute;top:50%;left:50%;margin-top:-384px;margin-left:-640px;width:1280px;height:768px;overflow:hidden;background-color:#4a4f6a;z-index:"+(df.zindexorder()+100)
				}
			})

			//-------------------------------------------

			this.LEDholder = df.newdom({
				tagname		: "div",
				target		: this.LEDwindow,
				attributes	: {
					id		: "LEDholder",
					style	: "position:absolute;margin-top:10px;left:907px;background-color:#000000;border-radius:5px;border:1px solid black;padding:5px;width:"+this.display_width+"px;height:"+this.display_height+"px;overflow:hidden;"
				},
				children	: [

				]
			})
			//-------------------------------------------

			this.LEDplugin = this.LEDpluginNPAPI = df.newdom({
				tagname		: "object",
				target		: this.LEDholder,
				attributes	: {
					id		: "LEDplugin",
					type	: "application/x-ledcontroller",
					style	: "position:absolute;margin-top:5px;left:5px;height:100px;width:100px;z-index:50000;"
				}
			})

/*
if (this.LEDplugin.openDebugConsole){
	this.LEDplugin.openDebugConsole()
} else {
console.log(">>>>>>>>>>>>>>>>>> NO LED PLUGIN")
	df.removedom(this.LEDplugin)
	this.LEDplugin = this.LEDpluginNPAPI = undefined
}
*/

			//-------------------------------------------

			this.COMplugin = {}/*df.newdom({
				tagname		: "object",
				target		: this.LEDholder,
				attributes	: {
					id		: "COMplugin",
					type	: "application/x-comadapter",
					style	: "position:absolute;margin-top:5px;left:5px;height:100px;width:100px;z-index:50000;"
				}
			})
*/

			//-------------------------------------------

			this.composite = df.newdom({
				tagname		: "canvas",
				target		: this.LEDwindow,
				attributes:{
					id		: "compositecanvas",
					width	: this.grid_width,
					height	: this.grid_height,
					style	: "position:fixed;top:-"+this.grid_width+"px;left:-"+this.grid_width+"px;display:none;"	//"position:fixed;top:20px;left:20px;border:1px solid white;"	//
				}
			})
			this.composite_ctx = this.composite.getContext("2d")

			//-------------------------------------------

			this.pluginwindow = df.newdom({
				tagname		: "div",
				target		: this.LEDwindow,
				attributes	: {
					id		: "pluginwindow",
					style	: "position:absolute;top:205px;left:380px;width:890px;height:550px;green;z-index:10;background-color:#6a7aaa;"
				}
			})


			//-------------------------------------------

			this.actionwindow = df.newdom({
				tagname		: "div",
				target		: this.LEDwindow,
				attributes	: {
					id		: "actionwindow",
					style	: "position:absolute;top:120px;left:380px;width:520px;height:82px;green;z-index:999999;"
				}
			})


			this.LEDbackground = df.newdom({
				tagname		: "div",
				target		: this.LEDholder,
				attributes	: {
					id		: "LEDbackground",
					style	: "width:"+this.display_width+"px;height:"+this.display_height+"px;position:relative;overflow:hidden;"
				}
			})

			//-------------------------------------------
			//not shown on screen
			this.contextHolder = df.newdom({
				tagname		: "div",
				target		: this.LEDwindow,
				attributes	: {
					style	: "position:absolute;top:-250px;left:380px;width:520px;height:87px;overflow:hidden;overflow-y:auto;background-color:#000000;"

				}
			})

			//-------------------------------------------

			this.buttonList = df.newdom({
				tagname	: "div",
				target	: this.LEDwindow,
				attributes	: {
					id		: "buttonList",
					style	: "position:absolute;top:10px;left:10px;width:350px;height:740px;",
					className: "buttonlist"
				},
				children	: [
					{
						tagname	: "ul",
						attributes	: {
							id	: "actionlist"
						}
					}
				]

			})
			//-------------------------------------------
			self.pluginConfig()

			df.attachevent({name:"beforeunload",target:window,handler:function(){
				for (var i=0;i<LED.queue.length;i++) {
					LED.stopPlugin(LED.plugins[i])
				}
			}})

			//-------------------------------------------
/*
			this.contextHolder = df.newdom({
				tagname		: "img",
				target		: this.LEDwindow,
				attributes	: {
					id		: "stopall",
					src		: "images/icon_stopall.png",
					style	: "position:absolute;top:11px;left:815px;width:85px;height:85px;cursor:pointer;"
				},
				events:[
					{
						name	: "click",
						handler	: LED.stopAll
					}
				]
			})
*/
			//-------------------------------------------

			if (this.plugins.length==0) {

				df.ajax.send({
					url	: "/FreakSelectorRunning",
					callback:function(){}
				})

				df.ajax.send({
					url : "/getFreakselectorPlugins",
					callback:function(data) {

						data = df.json.deserialize(data.text)


					var pluginlist = data.files
					var pluginlen = pluginlist.length

					df.waitfor(function(){
						return (pluginlen == self.plugins.length)
					},function(){
						LED.pluginsLoaded()
						self.drawPluginList()
						df.noselectDOM(self.LEDwindow,true)
					})

					var loadPlugin = function() {
/*
						setTimeout(function autoStartPlugins(){
							for (var ii=0;ii<LED.plugins.length;ii++){
								console.log(">>>>>>",LED.plugins[ii].autostart , LED.plugins[ii].started)
								if (LED.plugins[ii].autostart === true && LED.plugins[ii].started !== true) {
									LED.startPlugin(ii)//LED.plugins[ii].name
								}
							}
						}, 500);
*/
						if (pluginlist.length === 0) {
							return
						}

						var thisplugin = pluginlist.shift()
						
						df.ajax.xssloader({url:unescape(thisplugin).split("\\").pop()});


						//var new_script = document.createElement("script");
						//new_script.src = unescape(thisplugin).split("\\").pop();
						//document.getElementsByTagName("head")[0].appendChild(new_script)

						//setTimeout(loadPlugin,0)
						loadPlugin()
					}

					if (pluginlist.length>0) {
						loadPlugin()
					}

				}

			})
		}

//LED.startPlugin("Audio Input")
/*
LED.startPlugin("Music Player")
df.waitfor(function() {
	return (LED.registry["Music Player"].songlist && LED.registry["Music Player"].songlist.length>0)
},function(){
	LED.registry["Music Player"].selectSong(LED.registry["Music Player"].songlist[Math.floor(Math.random()*LED.registry["Music Player"].songlist.length)])
	setTimeout(function(){LED.startPlugin("Full EQ")},200)

})
*/


	}
	,

	addPlugin: function(plugin){
		LED.plugins.push(plugin);
		if (plugin.autostart === true && plugin.started !== true) {
			LED.startPlugin(LED.plugins.indexOf(plugin))//LED.plugins[ii].name
		}		
	},

	window_zoom: 1,
	
	allow_scaling: true,

	windowResize:	function(evtobj){
		var ui_aspect = 768 / 1280;

		var aspect = window.innerHeight / window.innerWidth;

		var newzoom

		if (aspect >= ui_aspect ){
			newzoom = (window.innerWidth / 1280 )
		} else {
			newzoom = (window.innerHeight / 768 )
			console.log(newzoom * window.innerWidth)
		}

		self.window_zoom = newzoom;
		window.window_zoom = newzoom;

		if (this.allow_scaling === true){
			df.dg("body").style.zoom = newzoom
		} else {
			df.dg("body").style.zoom = ""
		}
	},

	windowResize_working:	function(evtobj){
		var ui_aspect = 768 / 1280;

		var aspect = window.innerHeight / window.innerWidth;

		var newzoom

		if (aspect >= ui_aspect ){
			newzoom = (window.innerWidth / 1280 )
		} else {
			newzoom = (window.innerHeight / 768 )
			console.log(newzoom * window.innerWidth)
		}

		self.window_zoom = newzoom;
		window.window_zoom = newzoom;

		if (this.allow_scaling === true){
			df.dg("body").style.zoom = newzoom
		} else {
			df.dg("body").style.zoom = ""
		}
	}


	,
	pluginsLoaded:	function(){
		//load stored settings
		//start plugins marked as auto-start
		//etc..

		//set LCD brightness to 100
		df.ajax.send({
			url	: "nircmd/"+LED.base64encode("setbrightness 100 3")
		})

		//--------------------------------------------------------------
		//sort plugin list based on plugin type (group plugins)
		//this may also be over-ridden by a saved plugin layering preference

		LED.plugins.sort(function(a,b){

			var a_del = 0
			var b_del = 0

			if (a.top_plugin) {return -1}
			if (b.top_plugin) {return 1}

			if (a.fullscreen == true)	{a_del += 1}
			if (b.fullscreen == true)	{b_del += 1}

			if (a.render != undefined)	{a_del += 2}
			if (b.render != undefined)	{b_del += 2}

			if (a.has_audio == true || a.has_eqData == true)	{a_del += 4}
			if (b.has_audio == true || b.has_eqData == true)	{b_del += 4}

			if (a.render != undefined && a.fullscreen == true)	{a_del -= 2}
			if (b.render != undefined && b.fullscreen == true)	{b_del -= 2}

			if (a_del>b_del)		{return -1}
			if (a_del<b_del)		{return 1}

			if (a.name>b.name)		{return -1}
			if (a.name<b.name)		{return 1}

			return 0

		})

		//--------------------------------------------------------------


	},
//==================================================================================

	setSystemVolume:	function(newval){

		if (newval>1||newval<0) {
			return
		}

		try{clearTimeout(LED.sysvol_tmr)}catch(e){}
		LED.sysvol_tmr = setTimeout(function(){
			LED.system_volume = newval
			df.ajax.send({
				url	: "nircmd/"+LED.base64encode('setsysvolume '+(newval*65535))
			})
		},20)
	},
//==================================================================================

	master_volume	: 1,
	frame_time		: 34,

//==================================================================================

	GaussianKernel_sigma		: .5,
	GaussianKernel_maxError		: .01,
	ledType						: "B8R8G8",
	led_format					: "RGBA",	//RGBA  or BGR
	normalizeInput				: true,
	autoFit						: true,
	showInputImage				: false,
	setBilinearInterpolator		: true,
	ledRenderSize				: 3.08,

	hardware:	[
		{
			"name"	: "ViperBoard SPI",
			"type"	: "ViperBoardSPI",
			"config"	: {				// "CPOL_0,CPHA_0,CLK_3M"
				"CPOL"	: "CPOL_0",
				"CPHA"	: "CPHA_0",
				"CLK"	: "CLK_1M"//CLK_3M
			},
			"info":"Nano River Technologies - http://nanorivertech.com/"
		},

		{
			"name"	: "None",
			"type"	: "None",
			"config"	: {				// "CPOL_0,CPHA_0,CLK_3M"
			},
			"info":"None"
		},

		{
			"name"	: "Serial Port",
			"type"	: "ComPort",
			"config"	: {
				"Port:Bitrate"		: "COM9:115200",		// ":" comes after COM2 instead of comma :  "COM2:156000,8,NOPARITY,ONESTOPBIT", can we change it to comma too?
				"Data Bits"			: 8,
				"Parity"			: "NOPARITY",
				"Stop Bits"			: "ONESTOPBIT"
			},
			"info":"Standard RS232 Serial Port"
		}
/*		{
			"name"	: "Serial Port",
			"type"	: "ComPort",
			"config"	: {
				"Port:Bitrate"		: "COM9:921600",		// ":" comes after COM2 instead of comma :  "COM2:156000,8,NOPARITY,ONESTOPBIT", can we change it to comma too?
				"Data Bits"			: 8,
				"Parity"			: "NOPARITY",
				"Stop Bits"			: "ONESTOPBIT"
			},
			"info":"Standard RS232 Serial Port"
		}*/
	],

	pluginConfig:	function(){
		var self = window.LED

		var winwidth = 200

		var aspect = LED.grid_height / LED.grid_width

		//------------------------------------------------------------------------------------

		var openHardware = function(profile_idx){
			if (!self.LEDplugin){
				 self.canvasGrid()
			}


			self.hardware_device_idx = profile_idx

			var thishardware = self.hardware[profile_idx]
			if (self.LEDplugin.setLEDHardware && thishardware!=undefined) {
				self.LEDplugin.setLEDHardware(thishardware.type)
				var config = []
				for (var selector in thishardware.config) {
					config.push(thishardware.config[selector])
				}
console.log("hardware config = ",config.join(","))
				self.hardware_config = self.LEDplugin.setLEDHardwareConfig(config.join(","))
			}
		}
		
		//------------------------------------------------------------------------------------

		LED.viper_board_active = false

		if (self.urlParameters.dev!=true) {
			openHardware(0)
		}

console.log(self.hardware_config)

		self.hardware_active = (self.hardware_config == true)

		if (self.hardware_active != true) {	//self.LEDplugin.setBilinearInterpolator == undefined) {

			openHardware(1)

			self.hardware_active = (self.hardware_config == true)

			if (self.hardware_config == false) {
				self.urlParameters.dev = true
				self.hardware_active = false
			}
		//	self.canvasGrid()
		} else {
			LED.viper_board_active = true
		}

		if (self.urlParameters.dev == true) {
			LED.devmode = true
		}

		if (self.hardware_config != true && self.urlParameters.dev!=true) {
			alert("Output device not connected")
			//self.canvasGrid()
		}

//console.log("self.hardware_active",self.hardware_active,self.hardware_config_active)

		self.LEDplugin.ledType = LED.ledType

		self.LEDplugin.style.width = (self.grid_width*(self.display_width/self.grid_width))+"px"
		self.LEDplugin.style.height= ((self.grid_height*((self.display_width*aspect)/self.grid_height)))+"px"
		self.LEDplugin.style.top = "0px"

		self.LEDplugin.showInputImage = LED.showInputImage

		if (self.LEDplugin.setBilinearInterpolator == undefined) {return}

		self.LEDplugin.setBilinearInterpolator(LED.setBilinearInterpolator)
	//	self.LEDplugin.setGaussianKernel(self.GaussianKernel_sigma , self.GaussianKernel_maxError)

		//self.LEDplugin.autoFit = LED.autoFit	//if image input size is larger than the output size, the plugin will scale width and height to fit the output size

		self.LEDplugin.normalizeInput = self.normalizeInput

		LED.LEDplugin.ledGamma = self.ledGamma

		self.LEDplugin.setInputImageSize(self.grid_width,self.grid_height,LED.autoFit)
		//self.LEDplugin.setMonitorSize( self.grid_width*(self.display_height/self.grid_width) , self.grid_height*((176*aspect)/self.grid_height)  )
		self.LEDplugin.setMonitorSize( self.display_width,self.display_height )

//LED.LEDplugin.flipInputImageVertical = true
//LED.LEDplugin.flipInputImageHorizontal = true

		self.setMap()

		self.LEDplugin.ledRenderSize = self.ledRenderSize

	},

	canvasGrid:	function(){
		//create a dummy LED plugin, and a canavs to render on. The NPAPI plugin object usually renders the frames to the screen, but it may crash if the device is not connected.
		//to fix: allow NPAPI plugin to still work when device is detached.
alert("No LED Device, using canvasGrid for display.")
		var dummycanvas = df.newdom({
			tagname	: "canvas",
			target	: this.LEDbackground,
			attributes	: {
				id		: "dummycanvas",
				style	: "position:absolute;top:0px;left:0px;width:352px;height:176px;",
				width	: 352,
				height	: 176
			}
		})
		var dummy_ctx = dummycanvas.getContext("2d")

		this.LEDplugin = {
			ledGamma					: 0,
			ledAttenuation				: 1,
			redAttenuation				: 1,
			greenAttenuation			: 1,
			blueAttenuation				: 1,
			flipInputImageVertical		: false,
			flipInputImageHorizontal	: false,
			ledType						: "B8R8G8",
			ledGamma					: 1,

			style	:{
				width:0,
				height:0
			},
			addEventListener:			this.LEDpluginNPAPI ? this.LEDpluginNPAPI.addEventListener : function(){},
			addRGBProcess:				function(){},
			audioInputName:				this.LEDpluginNPAPI ? this.LEDpluginNPAPI.audioInputName : function(){},
			cancelAutoGrid:				function(){},
			clearRGBProcess:			function(){},
			clearVideoCaptureConfig:	function(){},
			enumerateAudioInputs:		this.LEDpluginNPAPI ? this.LEDpluginNPAPI.enumerateAudioInputs : function(){},
			fitInterestPointTransform:	function(){},
			getAudioData:				this.LEDpluginNPAPI ? this.LEDpluginNPAPI.getAudioData: function(){},
			getCaptureImage:			function(){},
			getLastException:			function(){},
			getVideoCaptureFrameRate:	function(){},
			initializeAudioInput:		this.LEDpluginNPAPI ? this.LEDpluginNPAPI.initializeAudioInput : function(){},
			ledUpdateRate:				function(){},
			ledUpdateSuccessRate:		function(){},
			openDebugConsole:			function(){},
			readAD:						function(){return -1},
			resetVideoCaptureConfig:	function(){},
			setAudioInputAutoScale:		this.LEDpluginNPAPI ? this.LEDpluginNPAPI.setAudioInputAutoScale : function(){},
			setBilinearInterpolator:	function(){},
			setConfiguration:			function(){},
			setConfiguration2:			function(){},
			setConfigurationFromFile:	function(){},
			setConfigurationFromFile2:	function(){},
			setGaussianKernel:			function(){},
			setInputImageSize:			function(){},
			setInterestPointParameters:	function(){},
			setInterestPointTracking:	function(){},
			setInterestPointTransform:	function(){},
			setLEDHardware:				function(){},
			setLEDHardwareConfig:		function(){},
			setMonitorSize:				function(){},
			setVideoCaptureConfig:		function(){},
			setVideoCaptureFrameStride:	function(){},
			setVideoCaptureSettings:	function(){},
			startAudioInput:			this.LEDpluginNPAPI ? this.LEDpluginNPAPI.startAudioInput : function(){},
			startAutoGrid:				function(){},
			startVideoCapture:			function(){},
			stopAudioInput:				this.LEDpluginNPAPI ? this.LEDpluginNPAPI.stopAudioInput : function(){},
			stopVideoCapture:			function(){},

			setInputImage:				function(framedata,led_format) {
				var data = LED.base64decode(framedata)

				var asepct = LED.grid_width/LED.grid_height

				var w = dummycanvas.width / LED.grid_width
				var h = dummycanvas.height / LED.grid_height

				for (var x=0;x<LED.grid_width;x++) {
					for (var y=0;y<LED.grid_height;y++) {

						var idx = LED.getCanvasIndex(x,y)
						var cr = Math.round(data.charCodeAt(idx) * this.ledAttenuation * this.redAttenuation)
						var cg = Math.round(data.charCodeAt(idx+1) * this.ledAttenuation * this.greenAttenuation)
						var cb = Math.round(data.charCodeAt(idx+2) * this.ledAttenuation * this.blueAttenuation)

						dummy_ctx.fillStyle = '#'+df.gethex(cr)+df.gethex(cg)+df.gethex(cb);

						dummy_ctx.fillRect(x*w, y*h, w, h);

					}
				}
				data.charCodeAt(0)


			}


		}

	},

	setMap:	function(){
		this.LEDplugin.setConfiguration(df.json.serialize(this.map))
	},


	setPixel:	function (x,y,r,g,b,a,context) {
		var frameidx = this.getCanvasIndex(x,y)
		var frame = context || this.frame
		frame[frameidx]   = String.fromCharCode(Math.floor(b))
		frame[frameidx+1] = String.fromCharCode(Math.floor(g))
		frame[frameidx+2] = String.fromCharCode(Math.floor(r))
		if (a) {
			frame[frameidx+3] = String.fromCharCode(Math.floor(a))
		}
	},
	setPixelHex:	function (x,y,hex,context) {
		var frameidx = this.getCanvasIndex(x,y) //((y*grid_width)+x)*4
		var r = df.getdec(df.hexcolor(hex).substr(0,2))
		var g = df.getdec(df.hexcolor(hex).substr(2,2))
		var b = df.getdec(df.hexcolor(hex).substr(4,2))
		var frame = context || this.frame
		frame[frameidx]   = String.fromCharCode(Math.floor(b))
		frame[frameidx+1] = String.fromCharCode(Math.floor(g))
		frame[frameidx+2] = String.fromCharCode(Math.floor(r))
	},

	getCanvasIndex:	function(x,y){
		return ((Math.floor(y)*LED.grid_width)+Math.floor(x))*4
	},

//=======================================================================================================================
//=======================================================================================================================
//=======================================================================================================================
//PLUGINS

	currentplugin		: 0,

	queue:[],	//The queue holds the plugins that are currently active. The order indicates which plugins get rendered on the context first, allowing compositing in layer order

//-----------------------------------------------------------------------

	drawPluginList:	function() {

		var self = this

		var list = df.dg("actionlist")
		list.innerHTML = ""

		var thiswidth = list.offsetWidth-6-20-22

		for (var i=0;i<this.plugins.length;i++) {

			var thisplugin = this.plugins[i]

			var html = []
			html.push('<span style="position:relative;float:left;width:100%;">')
			html.push(	((thisplugin.icon)?'<image src="freakselector_plugins/icons/'+thisplugin.icon+'" style="float:left;top:-5px;left:5px;position:absolute;z-index:0;"/>':'') )
			html.push(	'<span style="position:absolute;font-size:30px;margin-top:-4px;top:0pxp;left:0px;z-index:100;'+((thisplugin.icon)?'margin-left:55px;':'margin-left:10px;;font-size:30px;margin-top:-4px')+'">')
			html.push(		thisplugin.name )
			html.push(	'</span>')
			html.push(	'<span style="position:absolute;top:0pxp;left:0px;z-index:100;width:100%;">')
			html.push(	((thisplugin.fullscreen)?'<image title="Fullscreen" src="images/icon_small_fullscreen.png" style="float:right;z-index:0;margin-right:5px;width:20px;height:20px;"/>':'')	 )
			html.push(	((thisplugin.has_audio)?'<image title="Has Audio Data" src="images/icon_small_hasaudiodata.png" style="float:right;z-index:0;margin-right:5px;width:20px;height:20px;"/>':'')	 )
			html.push(	((thisplugin.has_eqData)?'<image title="Has EQ Data" src="images/icon_small_haseqdata.png" style="float:right;z-index:0;margin-right:5px;width:20px;height:20px;"/>':'')	 )
			html.push(	((thisplugin.render)?'<image title="Has Video" src="images/icon_small_hasvideo.png" style="float:right;z-index:0;margin-right:5px;width:20px;height:20px;"/>':'')	 )
			html.push(	((thisplugin.uses_camera)?'<image title="Uses Camera" src="images/icon_camera.png" style="float:right;z-index:0;margin-right:5px;width:20px;height:20px;"/>':'')	 )
			html.push(	'</span>')
			html.push('</span>')

			var thisbutton = df.newdom({
				tagname		: "div",
				target		: list,
				attributes	: {
					style	: "width:"+(thiswidth)+"px;",
					html	: html.join(""),
					className: (this.current_interface && this.plugins[i].name == this.current_interface.name)?"hasinterface":((this.plugins[i].started==true)?"playing":"")
				},
				events:[
					{
						name	: "click",
						handler	: function(evtobj){
							self.startPlugin(evtobj.srcdata.index)
							//evtobj.srcdata.plugin.interface()
						},
						index	: i,
						plugin	: thisplugin
					}
				]
			})

			thisplugin.actionbutton = thisbutton

			this.registry[thisplugin.name] = thisplugin

		}

	},

//-----------------------------------------------------------------------

	startPlugin:	function(index){

		var thisplugin = this.plugins[index] || this.registry[index]

//		if (thisplugin.render==undefined) {
//			thisplugin.start()
//			this.takeInterface(thisplugin)
//			this.actionButtonState()
//			return
//		}

		var queued = false
		var queued_idx
		for (var i=0;i<this.queue.length;i++){
			if (thisplugin.name == this.queue[i].name) {
				queued = true
				queued_idx = i
				break
			}
		}

		if (this.current_interface && this.current_interface.render == undefined && this.current_interface.has_eqData != true && this.current_interface.has_audio != true ) {
			var removefromqueue
			for (var i=0;i<this.queue.length;i++){
				if (this.current_interface.name == this.queue[i].name) {
					removefromqueue = i
					break
				}
			}
			if (removefromqueue!=undefined) {
				this.queue.splice(removefromqueue,1)
			}
		}

		if (queued != true) {
			if (thisplugin.fullscreen == true && this.queue.length>0) {
				if (this.queue[this.queue.length-1].fullscreen==true) {
					if (this.queue[this.queue.length-1].pause) {
						this.queue[this.queue.length-1].pause();
						this.queue[this.queue.length-1].paused = true
					}
					this.queue[this.queue.length-1].started = false
					this.queue.splice(this.queue.length-1,1)
				}
				this.queue.push(thisplugin)
				if (thisplugin.play && thisplugin.paused==true) {thisplugin.play();thisplugin.paused=false}

			} else {
				this.queue.unshift(thisplugin)
			}
		}

		if (this.queue.length == 1 && this.queue[0].render && this.bright_dimmer <1) {
			LED.fadeIn()
		}


		var startinterface = false
		if (thisplugin.started != true) {


			startinterface = ( thisplugin.start() != false )

			if (thisplugin.render || thisplugin.has_eqData || thisplugin.has_audio) {
				thisplugin.started = startinterface
			}

			if (this.queue.length >= 1){
				LED.toggleRender(true)
			}

		} else if (thisplugin.has_interface!=true) {
			startinterface = true
		}

		if (thisplugin.has_eqData==true) {
			if (LED.eqData_source != undefined) {
					if (LED.eqData_source.stopEQData) {LED.eqData_source.stopEQData()}
					var eqdata_queued = false
					var eqdata_queued_idx
					for (var i=0;i<this.queue.length;i++){
						if (LED.eqData_source.name == this.queue[i].name) {
							eqdata_queued = true
							eqdata_queued_idx = i
							break
						}
					}
			//		if (eqdata_queued == true) {
			//			this.queue[eqdata_queued_idx].started = false
			//			this.queue.splice(eqdata_queued_idx,1)
			//		}

					//TODO: turn on/off eq data icon highlighting

			}

			LED.eqData_source = thisplugin
		}

		if (startinterface!=false) {
			this.takeInterface(thisplugin)
		}

		this.actionButtonState()

		if (thisplugin.autostart && thisplugin.autoStartInit){
			setTimeout(thisplugin.autoStartInit,500)
		}

	},


	stopAll:	function() {

		LED.fadeOut(function(){
			for (var i=LED.queue.length-1;i>-1;i--) {
				if (LED.queue[i].render && LED.queue[i].stop){
					LED.queue[i].stop()
					LED.queue.splice(i,1)
				}
			}
			df.dg("pluginwindow").innerHTML = ""

		})

	},

	takeInterface:	function(thisplugin) {

		var self = this

		if (thisplugin.interface && thisplugin.has_interface!=true ) {
			if (this.current_interface){
				this.current_interface.has_interface = false
				if (this.current_interface.removeInterface) {this.current_interface.removeInterface()}
			}

			this.pluginwindow.innerHTML = ""
//			try{
				thisplugin.interface()
//			}catch(e){
//				console.error(e)
//			}
			thisplugin.has_interface = true
			this.current_interface = thisplugin
		}

		var firstlevel_left = 383
		if (thisplugin.fullscreen != true && thisplugin.render!=undefined && thisplugin.has_EQdata != true) {

			var pluginup = df.newdom({
				tagname	: "img",
				target	: LED.LEDwindow,
				attributes:{
					id	: "plugin_move_up",
					src:"images/icon_up.png",
					style:"cursor:pointer;position:absolute;top:10px;left:"+firstlevel_left+"px;width:55px;"
				},
				events:[
					{
						name:"click",
						handler:function(evt){

							var plugin_idx = -1
							for (var i=0;i<LED.plugins.length;i++){if (LED.current_interface.name == LED.plugins[i].name) {plugin_idx=i;break}}
							if (plugin_idx>0){
								var this_plugin = LED.plugins[plugin_idx]
								var next_plugin = LED.plugins[plugin_idx-1]
								LED.plugins[plugin_idx]   = next_plugin
								LED.plugins[plugin_idx-1] = this_plugin
							}

							var new_queue = []
							for (var i=0;i<LED.plugins.length;i++){
								if (LED.plugins[i].started == true) {
									new_queue.push(LED.plugins[i])
								}
							}
							LED.queue = new_queue

							var top = LED.buttonList.scrollTop
							LED.drawPluginList()
							LED.buttonList.scrollTop = top
						}
					}
				]
			})
			var plugindown = df.newdom({
				tagname	: "img",
				target	: LED.LEDwindow,
				attributes:{
					id	: "plugin_move_down",
					src:"images/icon_down.png",
					style:"cursor:pointer;position:absolute;top:60px;left:"+firstlevel_left+"px;width:55px;"
				},
				events:[
					{
						name:"click",
						handler:function(evt){

							var plugin_idx = -1
							for (var i=0;i<LED.plugins.length;i++){if (LED.current_interface.name == LED.plugins[i].name) {plugin_idx=i;break}}
							if (plugin_idx<LED.plugins.length-1){
								var this_plugin = LED.plugins[plugin_idx]
								var next_plugin = LED.plugins[plugin_idx+1]
								LED.plugins[plugin_idx]   = next_plugin
								LED.plugins[plugin_idx+1] = this_plugin
							}

							var new_queue = []
							for (var i=0;i<LED.plugins.length;i++){
								if (LED.plugins[i].started == true) {
									new_queue.push(LED.plugins[i])
								}
							}
							LED.queue = new_queue

							var top = LED.buttonList.scrollTop
							LED.drawPluginList()
							LED.buttonList.scrollTop = top
						}
					}
				]
			})
			firstlevel_left += 70
		} else {
			try{
				df.removedom(df.dg("plugin_move_up"))
				df.removedom(df.dg("plugin_move_down"))
			}catch(e){}
		}


		if (thisplugin.settings_to_save!=undefined) {

			var pluginup = df.newdom({
				tagname	: "img",
				target	: LED.LEDwindow,
				attributes:{
					id	: "plugin_settings",
					src:"images/settings.png",
					style:"cursor:pointer;position:absolute;top:10px;left:"+firstlevel_left+"px;width:95px;"
				},
				events:[
					{
						name:"click",
						handler:function(){
							LED.saveSettings()
						}
					}
				]
			})

			firstlevel_left += 100

		} else {
			if (df.dg("plugin_settings")){
				df.removedom(df.dg("plugin_settings"))
			}
		}

		if (thisplugin.play || thisplugin.stop || thisplugin.pause || thisplugin.next || thisplugin.last || thisplugin.mute  || thisplugin.end) {
			//df.setstyle("","","")
			//LED.pluginwindow.style.top = "100px"
			//df.setstyle(".buttonlist","height","628px")
			var actionwindow = df.dg("actionwindow")
			actionwindow.style.display="block"
			actionwindow.innerHTML = ""

			try{clearInterval(self.monitor_playstate)}catch(e){}

			if (thisplugin.play_state == undefined) {thisplugin.play_state = "playing"}

			var last,play,pause,stop,next,pluginup,plugindown


			if (thisplugin.end) {
				eject = df.newdom({
					tagname	: "img",
					target	: actionwindow,
					attributes:{
						src:"images/button_eject.png",
						style:"cursor:pointer;"
					},
					events:[
						{
							name:"click",
							handler:function(){
								LED.stopPlugin(thisplugin)
								if (LED.queue.length==0){
									LED.toggleRender()
								}

							}
						}
					]
				})
			}

			if (thisplugin.last) {
				last = df.newdom({
					tagname	: "img",
					target	: actionwindow,
					attributes:{
						src:"images/button_back.png",
						style:"cursor:pointer;"
					},
					events:[
						{
							name:"click",
							handler:function(){
								thisplugin.last()
							}
						}
					]
				})
			}

			if (thisplugin.play) {
				play = df.newdom({
					tagname	: "img",
					target	: actionwindow,
					attributes:{
						src:"images/button_play"+((thisplugin.play_state=="playing")?"_active":"")+".png",
						style:"cursor:pointer;"
					},
					events:[
						{
							name:"click",
							handler:function(evtobj){
								thisplugin.play()
								thisplugin.play_state = "playing"
								evtobj.target.src = "images/button_play_active.png"
								if (stop){stop.src="images/button_stop.png"}
								if (pause){pause.src="images/button_pause.png"}
							}
						}
					]
				})
			}

			if (thisplugin.pause) {
				pause = df.newdom({
					tagname	: "img",
					target	: actionwindow,
					attributes:{
						src:"images/button_pause"+((thisplugin.play_state=="paused")?"_active":"")+".png",
						style:"cursor:pointer;"
					},
					events:[
						{
							name:"click",
							handler:function(evtobj){
								thisplugin.pause()
								thisplugin.play_state = "paused"
								evtobj.target.src = "images/button_pause_active.png"
								if(stop){stop.src="images/button_stop.png"}
								if(play){play.src="images/button_play.png"}
							}
						}
					]
				})
			}

			if (thisplugin.stop) {
				stop = df.newdom({
					tagname	: "img",
					target	: actionwindow,
					attributes:{
						src:"images/button_stop"+((thisplugin.play_state=="stopped")?"_active":"")+".png",
						style:"cursor:pointer;"
					},
					events:[
						{
							name:"click",
							handler:function(evtobj){
								thisplugin.stop()
								thisplugin.play_state = "stopped"
								evtobj.target.src = "images/button_stop_active.png"
								if(play){play.src="images/button_play.png"}
								if(pause){pause.src="images/button_pause.png"}
							}
						}
					]
				})
			}

			if (thisplugin.next) {
				next = df.newdom({
					tagname	: "img",
					target	: actionwindow,
					attributes:{
						src:"images/button_ffwd.png",
						style:"cursor:pointer;"
					},
					events:[
						{
							name:"click",
							handler:function(){
								thisplugin.next()
							}
						}
					]
				})
			}



			self.monitor_playstate = setInterval(function(){

				if (play && thisplugin.play_state != "playing" && play.src.indexOf("images/button_play.png")==-1){play.src = "images/button_play.png"}
				if (stop && thisplugin.play_state == "stopped" && stop.src.indexOf("images/button_stop.png")==-1){stop.src="images/button_stop.png"}
				if (pause&& thisplugin.play_state == "paused"  && pause.src.indexOf("images/button_pause.png")==-1){pause.src="images/button_pause.png"}

				if (play && thisplugin.play_state == "playing" && play.src.indexOf("images/button_play_active.png")==-1){play.src = "images/button_play_active.png"}
				if (stop && thisplugin.play_state == "stopped" && stop.src.indexOf("images/button_stop_active.png")==-1){stop.src = "images/button_stop_active.png"}
				if (pause&& thisplugin.play_state == "paused" && pause.src.indexOf("images/button_pause_active.png")==-1){pause.src = "images/button_pause_active.png"}

			},100)


		} else {
		//	LED.pluginwindow.style.top = "0px"
			//df.setstyle(".buttonlist","height","738px")
			df.dg("actionwindow").style.display="none"
		}

		if (df.dg("saveSettingsDiv")!=null) {
			LED.saveSettings()
		}
	},

	actionButtonState:	function() {
		for (var i=0;i<LED.plugins.length;i++) {
			LED.plugins[i].actionbutton && LED.plugins[i].actionbutton.removeAttribute("class")
		}
		for (var i=0;i<LED.queue.length;i++) {
			if (LED.queue[i].started == true) {
				LED.queue[i].actionbutton && LED.queue[i].actionbutton.setAttribute("class","playing")
			}
			if (LED.queue[i].has_interface == true) {
				LED.queue[i].actionbutton && LED.queue[i].actionbutton.setAttribute("class","hasinterface")
			}
		}
	},

//-----------------------------------------------------------------------

	toggleRender:	function(force){

		var self = this

		if (force==true) {try{clearTimeout(this.render_tmr)}catch(e){};this.render_tmr = undefined}

		if (this.render_tmr == undefined || (force != undefined && force == true) ) {

			this.render_tmr = setTimeout(function(){self.render()} , this.frame_time)
		} else if (this.render_tmr != undefined || (force != undefined && force == false)) {
			try{clearTimeout(this.render_tmr)}catch(e){}
			this.render_tmr = undefined
			self.render()
		}

	},

	forceRender:	function() {
		try{clearTimeout(this.render_tmr)}catch(e){}
		this.render()
	},

//-----------------------------------------------------------------------
	render_tmr:undefined,

	testx:false,

	car_volume:1,

	is_hardware_connected: false,

	render:	function(){
		try{clearTimeout(this.render_tmr)}catch(e){}
		var self = this
		this.composite_ctx.clearRect(0, 0, this.grid_width,this.grid_height);
//console.log(LED.bright_dimmer)

		//-----------------------------------------
		var is_hardware_connected = (LED.LEDplugin.readAD(0) !== -2);
		if (!is_hardware_connected && self.is_hardware_connected === true){
			confirm("Viperboard Disconnected")
		} else if (is_hardware_connected && self.is_hardware_connected === false) {
			df.dompops.closepop()
		}
		self.is_hardware_connected = is_hardware_connected
		//-----------------------------------------

		if (self.capture_analog == true && is_hardware_connected !== -2) {
				self.Analog1 = 1 - LED.LEDplugin.readAD(0);
				self.Analog2 = 1 - LED.LEDplugin.readAD(1);
				self.Analog3 = 1 - LED.LEDplugin.readAD(2);
				self.Analog4 = 1 - LED.LEDplugin.readAD(3);

			if (self.Analog2!=-2){
				LED.bright_dimmer = self.Analog2
				LED.LEDplugin.ledAttenuation = self.Analog2
			}
			if (self.Analog1!=-1){
				LED.car_volume = self.Analog1
			}
		}

		var plugins_rendered = 0
		for (var i=this.queue.length-1;i>-1;i--) {
			try{
				if (this.queue[i].render != undefined){
					if (this.queue[i].output_plugin == true) {continue}
					this.queue[i].render()
					plugins_rendered++
				}
			}catch(e){
				console.warn("************************* Error rendering "+this.queue[i].name)
				console.error(e)
			}
		}

		for (var i=0;i<this.queue.length;i++) {
			if (this.queue[i].output_plugin == true) {
				this.queue[i].render()
			}
		}

		if (no_pixels_map.length>0){
			//if (no_pixels_map[obj.x][obj.y]==false){return}
			var image_data = LED.composite_ctx.getImageData(0,0,this.grid_width,this.grid_height)
			for (var inp=0;inp<no_pixels_map.length;inp++){
				LED.setPixel(no_pixels_map[inp].x,no_pixels_map[inp].y,0,0,0,0,image_data.data)
			}
			 LED.composite_ctx.putImageData(image_data, 0, 0);
		}

		if (this.queue.length == 0 && plugins_rendered == 0) {
			this.composite_ctx.clearRect(0, 0, this.grid_width,this.grid_height);
			this.sendframe()
		} else if (this.queue.length>0 && plugins_rendered>0) {
			this.sendframe()
		}

		if (LED.render_fast) {
			this.render_tmr = requestAnimationFrame(function(){self.render()} ,this.frame_time_override || this.frame_time)
		} else {
			this.render_tmr = setTimeout(function(){self.render()} , this.frame_time_override || this.frame_time)
		}

	},

//-----------------------------------------------------------------------

	analog_inputs:	[],

	lastframe:"",

	sendframe:	function(){
		var self = this

		var data = Array.apply(null,this.composite_ctx.getImageData(0,0,this.grid_width,this.grid_height).data)
		var frame = new Array(data.length)
		for (var i=0;i<data.length;i++) {frame[i] = String.fromCharCode(data[i])}
		var framedata = this.base64encode(frame.join(""))
//console.log(framedata)
		var result = this.LEDplugin.setInputImage(framedata, this.led_format)
/*
//adjusts frequency for slower output devices
		if (result==false) {
			this.frame_time+=100
			if (this.frame_time>400) {
				this.frame_time = 400
			}
		} else {
			this.frame_time-=25
			if (this.frame_time<10) {
				this.frame_time = 10
			}
		}
*/
//console.log(result, this.frame_time)
	},

//-----------------------------------------------------------------------
	fade_tmr	: undefined,
	fade_speed	: .04,
	fade_timeout	: 40,
	fade_inprogress	: false,
	fadeOut:	function(callback) {
		if (callback){
			callback()
			return
		}
		//disabling fadeout because it can cause problems
		return


		try{clearInterval(LED.fade_tmr)}catch(e){}
		LED.fade_inprogress = true
		LED.fade_tmr = setInterval(function() {
			if (LED.bright_dimmer > 0) {
				LED.bright_dimmer -= LED.fade_speed
				if (LED.bright_dimmer<0){LED.bright_dimmer=0}
				LED.LEDplugin.ledAttenuation = LED.bright_dimmer
				if (LED.bright_dimmer==0) {
					try{clearInterval(LED.fade_tmr)}catch(e){}
					LED.fade_inprogress = false
					if (callback) {callback()}
				}
//LED.LEDplugin.redAttenuation = LED.bright_dimmer
//LED.LEDplugin.greenAttenuation = LED.bright_dimmer
//LED.LEDplugin.blueAttenuation = LED.bright_dimmer
			} else {
				try{clearInterval(LED.fade_tmr)}catch(e){}
				if (callback) {callback()}
				LED.fade_inprogress = false
			}
		},LED.fade_timeout)
	},

	fadeIn:		function(callback) {
		if (callback){
			callback()
			return
		}
		//disabling fadein because it can cause problems
		return

		try{clearInterval(LED.fade_tmr)}catch(e){}
		LED.fade_inprogress = true
		LED.fade_tmr = setInterval(function() {
			if (LED.bright_dimmer < 1) {
				LED.bright_dimmer += LED.fade_speed
				if (LED.bright_dimmer>1){
					LED.bright_dimmer=1
				}
				LED.LEDplugin.ledAttenuation = LED.bright_dimmer
				if (LED.bright_dimmer==1) {
					try{clearInterval(LED.fade_tmr)}catch(e){}
					LED.fade_inprogress = false
					if (callback) {callback()}
				}
//LED.LEDplugin.redAttenuation = LED.bright_dimmer
//LED.LEDplugin.greenAttenuation = LED.bright_dimmer
//LED.LEDplugin.blueAttenuation = LED.bright_dimmer
			} else {
				try{clearInterval(LED.fade_tmr)}catch(e){}
				if (callback) {callback()}
				LED.fade_inprogress = false
			}
		},LED.fade_timeout)
	},

//-----------------------------------------------------------------------

	stopPlugin:	function(index,faded) {
		var thisplugin = this.registry[index.name] || this.plugins[index] || this.registry[index]

		if (faded!=true && LED.queue.length==1 && LED.queue[0].render != undefined) {
			LED.fadeOut(function() {
				LED.stopPlugin(index,true)
			})
			return
		}

		thisplugin.started = false

		if (LED.current_interface == thisplugin) {
			LED.pluginwindow.innerHTML = ""
			thisplugin.has_interface = false
			LED.current_interface = undefined
		}

		if (LED.eqData_source == thisplugin) {
			LED.eqData_source = undefined
		}


		thisplugin.actionbutton.className=""

		LED.actionwindow.innerHTML = ""

		if (thisplugin.end){thisplugin.end()}

		for (var i=LED.queue.length-1;i>-1;i--) {
			if (LED.queue[i].name === thisplugin.name) {
				LED.queue.splice(i,1)
				break
			}
		}


	},

//=======================================================================================================================


	getTree:	function(thispath,goback,target,thiscallback) {

		var self = this

		if (thiscallback==undefined) {thiscallback = self.renderTree}

		var thistree = []

		if (goback!=undefined && goback.push) {
			goback.push(thispath)
		}

		df.ajax.send({
			url : "gettree.aspx?thispath="+thispath,
			callback:function(data) {

				var tree = df.json.deserialize(data.text)

				if (thiscallback!=undefined) {thiscallback(tree,target,goback)}

			}
		})
	},


	renderTree:	function(tree,target,goback,select_callback,current) {

		var self = this

		var playlist = target

		var folders = tree.folders

		target.innerHTML=""

		//if (self.goback.length>1) {
		if (goback[goback.length-1]!=undefined){
			df.newdom({
				tagname	: "li",
				target	: target,
				attributes:{
					className: "folderlist",
					html	: '[Back] <span style="color:#ffffff;">'+unescape(goback[goback.length-1])+'</span>'
				},
				events:	[
					{
						name	: "mouseup",
						handler	: function(evtobj) {
							if (goback.length>0) {
								goback.pop()
								var treeback = goback.pop()
console.log("treeback",treeback)
								LED.getTree(treeback,goback,target,self.renderTree)
							}

/*
							if (goback.length==0) {
								tree = {
									folders:{
										"D:%5Cmusic":[],
										"E:%5Cmusic2":[]
									},
									files:[]
								}
								self.renderTree()
								return
							}
*/

						}
					}
				]
			})
		}
		//}

		for (var selector in folders) {
			df.newdom({
				tagname	: "li",
				target	: target,
				attributes:{
					className: "folderlist"+((current && current.indexOf(selector)!=-1)?" playingfolder":""),
					html	: unescape(selector.replace(/_/g," "))
				},
				events:	[
					{
						name	: "mouseup",
						handler	: function(evtobj) {
							var folder = evtobj.srcdata.selector
							LED.getTree(folder,goback,target)

						},
						selector		: selector
					}
				]
			})
		}

		var current_file
		for (var i=0;i<tree.files.length;i++) {
			var thisfilebutton = df.newdom({
				tagname	: "li",
				target	: target,
				attributes:{
					className: "folderbutton"+((self.currently_playing == tree.files[i])?" playing":""),
					html	: unescape(tree.files[i].replace(/_/g," ")).substr(unescape(tree.files[i].replace(/_/g," ")).lastIndexOf("\\")+1)
				},
				events:	[
					{
						name	: "mouseup",
						handler	: function(evtobj) {
							var thisfile = tree.files[evtobj.srcdata.idx]
							if (select_callback) {select_callback( unescape(music).substr(3), thisfile )}
							LED.renderTree(tree,target,goback,select_callback,current)
						},
						idx		: i
					}
				]
			})
			if (current==tree.files[i]) {
				current_file = thisfilebutton
			}
		}

		if (current_file!=undefined){
			current_file.scrollIntoView()
		}

	},


//=======================================================================================================================
	sliderList:	function(slidertypes) {

		var sliders = []
		
		var thisvalue;

		for (var i=0;i<slidertypes.length;i++) {

			sliders.push(
				{
					tagname	: "div",
					attributes:{
						style	: "clear:both;position:relative;padding:3px;margin:4px;border:1px solid black;border-radius:5px;background-color:#2a2a4a;font-family:arial;font-size:12px;"
					},
					children:[
						{
							tagname	: "div",
							attributes	: {
								html	: slidertypes[i].name,
								style	: "font-size:20px;margin-bottom:10px;position:relative;top:5px;left:5px;"
							}
						},
						thisvalue = {
							tagname	: "div",
							attributes	: {
								id	: "slidervalue_"+slidertypes[i].id,
								html	: parseFloat(slidertypes[i].startvaluetext||slidertypes[i].startvalue||0).toFixed(2),
								style	: "font-size:20px;position:absolute;top:5px;left:-5px;text-align:right;width:100%;"
							}
						},
						{
							tagname	: "df-slider",
							attributes	: {
								id			: "slider_"+slidertypes[i].id,
								name		: slidertypes[i].name,
								className	: slidertypes[i].classname||"slider",
								min			: (slidertypes[i].min!=undefined)?slidertypes[i].min:0,
								max			: (slidertypes[i].max!=undefined)?slidertypes[i].max:1,
								startvalue	: slidertypes[i].startvalue||0,
								height		: (slidertypes[i].height!=undefined)?slidertypes[i].height:44,
								width		: (slidertypes[i].width!=undefined)?slidertypes[i].width:285,
								onchange	: slidertypes[i].handler,
								onmousedown	: slidertypes[i].mousedown,
								onmouseup	: slidertypes[i].mouseup,
								style		: slidertypes[i].style||"",
								showvalue	: thisvalue.attributes.id
							}
						}
					]
				}

			)
		}

		return sliders
	},
//=======================================================================================================================
	settings_to_save:[
		"capture_analog",
		"rdimmer",
		"gdimmer",
		"bdimmer",
		"bright_dimmer",
		"ledGamma"

	],

	capture_analog: false,

	rdimmer		: 1,
	gdimmer		: 1,
	bdimmer		: 1,
	bright_dimmer	: 1,
	ledGamma	: 2,

	settings_global_save:false,

	saveSettings:	function(){

		var self = this

		var savedom = df.newdom({
			tagname	: "div",
			target	: this.LEDwindow ,//LED.pluginwindow,
			attributes:{
				id	: "saveSettingsDiv",
				style:	"position:absolute;z-index:999999;top:44px;left:377px;width:520px;height:100px;background-color:#eaeaea;border-radius:20px;border:5px solid black;",
				html	: ""
			}
		})

		df.newdom({
			tagname	: "div",
			target	: savedom,
			attributes	: {
				html	:"Saved Settings",
				style	: "position:absolute;top:9px;left:7px;font-size:24px;color:black;font-family:arial;"
			}
		})

		var plugin_settings

		var thisstorage = localStorage.getItem("savedSettings")
		if (thisstorage == null) {
			localStorage.setItem("savedSettings","{}")
			plugin_settings = {}
		} else {
			plugin_settings = df.json.deserialize(thisstorage)
		}

		var has_settings = false
		for (var selector in plugin_settings) {
			has_settings = true
		}

		if (has_settings) {

			var optionslist = []
			for (var selector in plugin_settings) {
				if (LED.settings_global_save != true && plugin_settings[selector][LED.current_interface.name]==undefined) {
					continue
				}

				optionslist.push(df.newdom({
					tagname	: "option",
					attributes:{
						value	: selector,
						html	: selector
					}
				}))
			}

			var saveselect = df.newdom({
				tagname	: "select",
				target	: savedom,
				attributes	: {
					style	: "position:absolute;top:43px;width:368px;left:11px;font-size:30px;color:black;font-family:arial;"
				},
				children:optionslist,
				events:[
					{
						name:["change","click"],
						handler:function(){
							savename.value = saveselect.getElementsByTagName("option")[saveselect.selectedIndex].value
						}
					}
				]
			})

		}

		var savename = df.newdom({
			tagname	: "input",
			target	: savedom,
			attributes	: {
				value	: "",
				type	: "text",
				style	: "position:absolute;top:43px;width:350px;left:11px;font-size:30px;color:black;font-family:arial;"
			}
		})


		df.newdom({
			tagname	: "input",
			target	: savedom,
			attributes	: {
				value	: "SAVE",
				type	: "button",
				style	: "position:absolute;top:4px;width:84px;left:179px;font-size:18px;color:black;font-family:arial;"
			},
			events	: [
				{
					name	: "click",
					handler	: function(evt) {

						if (savename.value=="") {
							alert("Please enter a name for these settings")
							return
						}

						var save_global = global_setting.getState()

						var settingsobj = {}

						if (save_global == true) {
							settingsobj["LED"] = {
								settings:{}
							}
							for (var ix=0;ix<LED.settings_to_save.length;ix++) {
								settingsobj["LED"].settings[ LED.settings_to_save[ix] ] = LED[LED.settings_to_save[ix]]
							}
						}

						for (var regname in LED.registry) {

							var thisplugin = LED.registry[regname]

							if (save_global != true && thisplugin.has_interface != true) {continue}

							if (thisplugin.settings_to_save) {

								settingsobj[regname] = {
									settings:{}
								}
								for (var ix=0;ix<thisplugin.settings_to_save.length;ix++) {
									settingsobj[thisplugin.name].settings[ thisplugin.settings_to_save[ix] ] = thisplugin[thisplugin.settings_to_save[ix]]
								}
							}
						}

						var savedSettings = df.json.deserialize(localStorage.getItem("savedSettings"))
						savedSettings[savename.value] = settingsobj
						localStorage.setItem("savedSettings",df.json.serialize(savedSettings))

						self.saveSettings()
					}
				}
			]
		})

		if (has_settings) {
			df.newdom({
				tagname	: "input",
				target	: savedom,
				attributes	: {
					value	: "LOAD",
					type	: "button",
					style	: "position:absolute;top:4px;width:84px;left:274px;font-size:18px;color:black;font-family:arial;"
				},
				events	: [
					{
						name	: "click",
						handler	: function(evt) {

							if (savename.value=="") {
								alert("Please select a setting to load")
								return
							}

							var load_global = global_setting.getState()

							var savedSettings = df.json.deserialize(localStorage.getItem("savedSettings"))

							var loaded_settings = savedSettings[savename.value]
console.log(setting)
							for (var plugin in loaded_settings) {
								if (plugin == "LED") {
									var thisplugin = window.LED
								} else {
									var thisplugin = LED.registry[plugin]
								}

								if (load_global != true && thisplugin.has_interface != true) {continue}

								for (var setting in loaded_settings[plugin].settings) {
									thisplugin[setting] = loaded_settings[plugin].settings[setting]
								}

								if (thisplugin.settingsUpdate != undefined) {
									thisplugin.settingsUpdate()
								}

							}

							var settings_open = (df.dg("saveSettingsDiv")!=null)

							LED.pluginwindow.innerHTML = ""
							LED.current_interface.interface()

							if (settings_open==true) {
								LED.saveSettings()
							}
						}
					}
				]
			})


			df.newdom({
				tagname	: "input",
				target	: savedom,
				attributes	: {
					value	: "DELETE",
					type	: "button",
					style	: "position:absolute;top:4px;width:84px;left:372px;font-size:18px;color:black;font-family:arial;"
				},
				events	: [
					{
						name	: "click",
						handler	: function(evt) {

							if (savename.value=="") {
								alert("Please select a setting to delete")
								return
							}
							var savedSettings = df.json.deserialize(localStorage.getItem("savedSettings"))

							delete savedSettings[savename.value]

							localStorage.setItem("savedSettings",df.json.serialize(savedSettings))

							self.saveSettings()



						}
					}
				]
			})
		}

		df.newdom({
			tagname	: "div",
			target	: savedom,
			attributes	: {
				style	: "position:absolute;border:2px solid black;top:4px;border-radius:20px;left:100%;margin-left:-40px;background-color:red;font-size:24px;width:25px;height:27px;padding-top:3px;padding-left:3px;padding-right:2px;cursor:pointer;",
				html	: '\u2716'
			},
			events	: [
				{
					name	: "click",
					handler	: function(evt) {
						df.removedom(savedom)
					}
				}
			]

		})


		var global_setting = df.newdom({
			tagname		:"df-onoff",
			target		: savedom,
			attributes:{
				id		: "globalsettings",
				name		: '<span style="float:right;margin-top:1px;">Global</span>',
				namestyle	: "width:100px;font-size:20px;",
				style		: "position:absolute;top:46px;left:396px;",
				className	: "onoff",
				width		: 25,
				value		: LED.settings_global_save,
				onchange	: function(val){
					LED.settings_global_save = val
					LED.saveSettings()
				}
			}
		})


	},

	loadSettings:	function(plugin){


	},
//=======================================================================================================================

	reorderQueue:	function(){
	},

	soundlocked:undefined,	//need a property to track which plugin has ownership of the sound

	registry:	{}	//makes it possible to access plugisn by LED.registry["plugin name"]
	,
	plugins:[],

	music:{
		eqData:[],
		waveformData:{left:[],right:[]},
		peakData:{left:0.0,right:0.0},
		juice:{
			frameCount : 0,
			volume : 0,
			freqBands : [],
			freqBands16 : [],
			avgFreqBands : [],
			relFreqBands : [],
			longAvgFreqBands : [],
			avgRelFreqBands : [],
			waveData : [],
			eqData : [],
			freqData : []

		}

	},	//soundManager2 music Object, or Audio Input emulated soundmanager2 Object, or other input with eq and waveform data

//=======================================================================================================================

	fontlist: [
		'Arial',
		'Arial Bold',
		'Courier',
		'Calibri',
		'Cambria',
		'Courier New',
		'Terminal',
		'Tahoma',
		'Times New Roman',
		'Trebuchet MS',
		'Verdana',
		'Impact Regular',
		'Franklin Gothic',
		'Georgia',
		'Fixedsys Regular',
		'BanksiaBold',
		'BanksiaBlack',
		'BanksiaRegular',
		'BurnstownDamRegular',
		'ChunkFiveRoman',
		'DaysRegular',
		'FirecatMedium',
		'GongNormal',
		'GreenFuzRegular',
		'JungleFeverRegular',
		'PincoyablackBlack',
		'RadiolandRegular',
		'RadiolandSlimRegular',
		'LCDBold',
		'TinetRegular',
		'WhitehallRegular'
	]
};

//=======================================================================================================================
//=======================================================================================================================
//=======================================================================================================================

df.attachevent({
	name:"load",
	target:window,
	handler:function(){
		LED.start(testgrid)
	}
});

//=======================================================================================================================

(function(){

//auto-scroller - allows dragging of DIVs with an overflow-auto-y or overflow-scroll-y for touch devices

	var touchsrcoll
	var	touchsrcoll_click
	var	touchsrcoll_mouseup
	var	touchsrcoll_mouseout

	var touchsrcoll_startx
	var touchsrcoll_starty

	var lastscrollTop
	var lastscrollLeft

	var hasmoved = 0

	df.attachevent({
		target	: window,
		name	: "mousedown",
		handler	: function(evt) {

			if (evt.srcElement.tagName.toLowerCase() == "textarea"){
				return
			}

			if (df.mouse.xdrag!=undefined&&df.mouse.xdrag!="") {return}

			var thisobj = evt.target

			while (thisobj!=document && thisobj.tagName!=undefined && thisobj.tagName.toLowerCase()!="html" && thisobj.tagName.toLowerCase()!="body") {
				var computed
				try{computed = df.getComputedStyle(thisobj,"overflow-y")||df.getComputedStyle(thisobj,"overflowY")}catch(e){computed=""}
				if (computed=="auto"||computed=="scroll") {
					break
				}
				thisobj = thisobj.parentNode
			}

			if (thisobj==document || thisobj.tagName==undefined || thisobj.tagName.toLowerCase()=="html" || thisobj.tagName.toLowerCase()=="body") {return}

			touchsrcoll = thisobj
			touchsrcoll_click = thisobj
			touchsrcoll_mouseup = thisobj
			touchsrcoll_mouseout = thisobj


			touchsrcoll_startx	= evt.clientX
			touchsrcoll_starty	= evt.clientY

			touchsrcoll_scrollTop = touchsrcoll.scrollTop
			touchsrcoll_scrollLeft= touchsrcoll.scrollLeft

			lastscrollTop = touchsrcoll.scrollTop
			lastscrollLeft= touchsrcoll.scrollLeft

			hasmoved = 0

			df.killevent(evt)

		}
	})

	df.attachevent({
		target	: window,
		name	: "mousemove",
		bubble	: true,
		handler	: function(evt) {

			if (evt.which!=1) {
				touchsrcoll_click = undefined
				touchsrcoll = undefined
				touchsrcoll_mouseup=undefined
				return
			}

			if (touchsrcoll==undefined||touchsrcoll_click==undefined||touchsrcoll_mouseup==undefined) {
				touchsrcoll = undefined
				return
			}

			if (lastscrollTop!=touchsrcoll.scrollTop || lastscrollLeft!=touchsrcoll.scrollLeft) {touchsrcoll=undefined;return}
				hasmoved++

			setTimeout(function(){

				if (touchsrcoll==undefined) {return}

				var offset_x = evt.clientX - touchsrcoll_startx
				var offset_y = evt.clientY - touchsrcoll_starty

				touchsrcoll.scrollTop = touchsrcoll_scrollTop - offset_y
				touchsrcoll.scrollLeft= touchsrcoll_scrollLeft- offset_x

				lastscrollTop = touchsrcoll.scrollTop
				lastscrollLeft= touchsrcoll.scrollLeft


			},6)

		}
	})


	df.attachevent({
		target	: window,
		name	: "click",
		bubble	: true,
		handler	: function(evt) {
			if (touchsrcoll_click!=undefined && hasmoved>5) {
				evt.cancelBubble = true
				df.killevent(evt)
				touchsrcoll_click = undefined
				return false
			}

			touchsrcoll_click = undefined

		}
	})

	df.attachevent({
		target	: window,
		name	: "mouseup",
		bubble	: true,
		handler	: function(evt) {
			if (touchsrcoll_mouseup!=undefined && hasmoved>5) {
				evt.cancelBubble = true
				df.killevent(evt)
				touchsrcoll_mouseup = undefined
				return false
			}

			touchsrcoll_mouseup=undefined


		}
	})




})()

//=======================================================================================================================
//=======================================================================================================================
/*! Copyright (C) 1999 Masanao Izumo <iz@onicos.co.jp>
 * Version: 1.0
 * LastModified: Dec 25 1999
 * This library is free.  You can redistribute it and/or modify it.
 */
LED.base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
LED.base64DecodeChars = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,62,-1,-1,-1,63,52,53,54,55,56,57,58,59,60,61,-1,-1,-1,-1,-1,-1,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,-1,-1,-1,-1,-1,-1,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,-1,-1,-1,-1,-1]
LED.base64encode = function(str) {var out, i, len; var c1, c2, c3; len = str.length; i = 0; out = []; while(i < len) { c1 = str.charCodeAt(i++) & 0xff; if(i == len) { out.push(this.base64EncodeChars.charAt(c1 >> 2)); out.push(this.base64EncodeChars.charAt((c1 & 0x3) << 4)); out.push("=="); break; }; c2 = str.charCodeAt(i++); if(i == len){ out.push(this.base64EncodeChars.charAt(c1 >> 2)); out.push(this.base64EncodeChars.charAt(((c1 & 0x3)<< 4) | ((c2 & 0xF0) >> 4))); out.push(this.base64EncodeChars.charAt((c2 & 0xF) << 2)); out.push("="); break; }; c3 = str.charCodeAt(i++); out.push(this.base64EncodeChars.charAt(c1 >> 2)); out.push(this.base64EncodeChars.charAt(((c1 & 0x3)<< 4) | ((c2 & 0xF0) >> 4))); out.push(this.base64EncodeChars.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >>6))); out.push(this.base64EncodeChars.charAt(c3 & 0x3F)); }; return out.join(""); }
LED.base64decode = function(str) {var c1, c2, c3, c4; var i, len, out; len = str.length; i = 0; out = []; while(i < len) { do { c1 = this.base64DecodeChars[str.charCodeAt(i++) & 0xff]; } while(i < len && c1 == -1); if(c1 == -1) break; do { c2 = this.base64DecodeChars[str.charCodeAt(i++) & 0xff]; } while(i < len && c2 == -1); if(c2 == -1) break; out.push( String.fromCharCode((c1 << 2) | ((c2 & 0x30) >> 4)) ); do { c3 = str.charCodeAt(i++) & 0xff; if(c3 == 61) return out.join(""); c3 = this.base64DecodeChars[c3]; } while(i < len && c3 == -1); if(c3 == -1) break; out.push( String.fromCharCode(((c2 & 0XF) << 4) | ((c3 & 0x3C) >> 2)) ); do { c4 = str.charCodeAt(i++) & 0xff; if(c4 == 61) return out.join(""); c4 = this.base64DecodeChars[c4]; } while(i < len && c4 == -1); if(c4 == -1) break; out.push( String.fromCharCode(((c3 & 0x03) << 6) | c4) ); } return out.join("")}


var max = Math.max;
var min = Math.min;
var _pow = Math.pow;
var pow = function(n, p) { var r = _pow(n, p); return isNaN(r) ? 0 : r; };
var sqr = function(n) { return n*n; };
var sqrt = Math.sqrt;
var sin = Math.sin;
var cos = Math.cos;
var atan2 = Math.atan2;
var tan = Math.tan;
var int = Math.floor;
var asin = Math.asin;
var atan = Math.atan;
var acos = Math.acos;
var log = Math.log;
var log10 = function(n) { return log(n) / log(10); };
var sign = function(n) { return n < 0 ? -1 : (n == 0 ? 0 : 1); };
var exp = Math.exp;
var int = Math.floor;
var abs = Math.abs;
var ternif = function(cond,val1,val2) { return cond ? val1 : val2; };
var bor = function(n1, n2) { return (n1 || n2) ? 1 : 0; };
var band = function(n1, n2) { return (n1 && n2) ? 1 : 0; };
var bnot = function(n1) { return n1 ? 0 : 1; };
// https://xbmc.svn.sourceforge.net/svnroot/xbmc/vendor/libprojectM/current/BuiltinFuncs.hpp
var R =  32767, RR = 65534;
var sigmoid = function(n1, n2) { return (RR / (1 + exp( -(n1 * n2) / R) - R)) };
var random = Math.random;
function rand(num) {
	return random() * num;
}
var equal = function(a,b) { return (a == b) ? 1 : 0; };
var above = function(a,b) { return (a > b) ? 1 : 0; };
var below = function(a,b) { return (a < b) ? 1 : 0; };
var PI = Math.PI;
var PI2 = Math.PI * 2;

//=======================================================================================================================
//=======================================================================================================================

function xhr(url, callback, error, method) {
	var http = null;
	if (window.XMLHttpRequest) {
		http = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		http = new ActiveXObject("Microsoft.XMLHTTP");
	}
	if (http) {
		if (callback) {
			if (typeof(http.onload) != "undefined")
				http.onload = function() {
					callback(http);
					http = null;
				};
			else {
				http.onreadystatechange = function() {
					if (http.readyState == 4) {
						callback(http);
						http = null;
					}
				};
			}
		}
		http.open(method || "GET", url + "?time=" + new Date().getTime(), true);
		http.send(null);
	} else {
		if (error) error();
	}
}

//=======================================================================================================================




//=============================================================================

function autoMap(){
	var size = 173


	var pixels=[]
	for (var i=0;i<size;i++){
		pixels.push({idx:i,on:false})
	}

	var position = 0

	function nextPixel(){

		for (var i=0;i<size;i++){
			pixels[i].on = false
		}

		if (position>=173){
			test.newdata_callback = undefined
			sendAutoFrame(pixels)
			return
		}

		pixels[position].on = true

		sendAutoFrame(pixels)

		position++

	}
	test.newdata_callback = nextPixel

	nextPixel()
}

function sendAutoFrame(pixels){
	var pixeldata = []
	for (var i=0;i<pixels.length;i++){
		var thiscolor = (pixels[i].on)?"ffffff":"000000"

		test.pixel_test[i].style.backgroundColor = "#"+thiscolor
		var this_r = df.getdec(thiscolor.substr(0,2))
		var this_g = df.getdec(thiscolor.substr(2,2))
		var this_b = df.getdec(thiscolor.substr(4,2))

		pixeldata.push(String.fromCharCode(this_b))
		pixeldata.push(String.fromCharCode(this_r))
		pixeldata.push(String.fromCharCode(this_g))
	}

	var dataframe = window.btoa("MCX"+pixeldata.join(""))

	test.opencom.writeAsync(dataframe,true)
}

