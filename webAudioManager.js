;(function(){
/*

Chrome 46 has a bug that causes playback problems in html5 audio. This is a replacement for soundManager functionality used in Splice Sounds.

The following functionality is supported:

  createSound(options)

    options.url
    options.onresume()
    options.onplay()
    options.onpause()
    options.onstop()
    options.whileplaying()
    options.onfinish()
    options.onload()

  returns a 'sound' object.

    sound.duration
    sound.position
    sound.paused
    sound.playState
    sound.setPosition()
    sound.destruct()
    sound.togglePause()
    sound.stop()
    sound.plaY()

Look here for more information and resolution:  https://code.google.com/p/chromium/issues/detail?id=543808

*/

/*
Need to implement:

	//var queue = window.soundManager.sounds
	//var queue = window.soundManager.sounds
	  var music = soundManager.createSound({
	//soundManager.stopAll();
	  if (self.current_sound){soundManager.stop(self.current_sound)}
	  soundManager.pause(LED.registry["Sound Effects"].music.sID)
	  soundManager.play(LED.registry["Sound Effects"].music.sID)
	  soundManager.destroySound(self.music.sID)
	//soundManager.debugMode = true;
	  soundManager.onload = function() {
	 
	  music.juice.avgFreqBands
	  music.juice.longAvgFreqBands
	  music.eqData
	  music.peakData
	  music.waveformData
	  music.sID
	  music.waveDataL
	  music.waveDataR
	  music.stop(sID)
	  music.play(sID)
	  music.whileplaying=function
	  music.unload()
	  music.playState
	  music.paused
	  music.bytesTotal
	  music.bytesLoaded
	  music.position
	  music.duration
	  music.durationEstimate
	  music.setVolume()//0-100
	  

*/

  var webManagerAudioContext;

  var audio_context = window.AudioContext||window.webkitAudioContext;

  if (audio_context){
    webManagerAudioContext = new audio_context();//this is intended only as a short-term chrome-only solution at the moment.
  }

  var base64ToBuffer = function (buffer) {
    if (buffer.indexOf("data:audio/mp3;base64,")!=-1){
      buffer = buffer.substr(22)
    }
    var binary = window.atob(buffer);
    var buffer = new ArrayBuffer(binary.length);
    var bytes = new Uint8Array(buffer);
    for (var i = 0; i < buffer.byteLength; i++) {
        bytes[i] = binary.charCodeAt(i) & 0xFF;
    }
    return buffer;
  };

  var all_sounds = [];

  function stopAll(){
    for (var i=0;i<all_sounds.length;i++){
      all_sounds[i].stop();
    }
  }

window.webAudioManager = {

  createSound:function(options){

    var sound = {}

    var data_uri = options.url;

    var sound_arrayBuffer = base64ToBuffer(data_uri)

    var play_tmr;
    var start_time;
    var sound_source;
    var audio_buffer;
    var wants_to_play;

    sound.position = 0;
    sound.duration = 0;
    sound.playState = 0;
    sound.paused = false;
    sound.start_offset = 0;


    sound.play = function(start_offset) {

      sound.start_offset = start_offset || 0;

      sound.position = start_offset || 0;

      stopAll()

      if (!audio_buffer){
        wants_to_play = true;
        return
      }

      wants_to_play = undefined;

      sound.source = webManagerAudioContext.createBufferSource();

      sound.source.buffer = audio_buffer; 

      sound.source.loop = false;
      sound.source.connect(webManagerAudioContext.destination);
      sound.duration = sound.source.buffer.duration * 1000;

      sound.source.onended = function(){
        stopPlayTimer();
        if (sound){ sound.playState = 0 };
        
        if (options.onfinish){options.onfinish()}

        sound.source = undefined;
      }

      var play_time = webManagerAudioContext.currentTime;
      sound.source.start(play_time, sound.position / 1000);
      start_time = (play_time * 1000);
      sound.playState = 1;

      whilePlaying()

      if (options.onplay){options.onplay()}

    }

    sound.setPosition = function(offset){
      sound.position = offset;
      if (sound.playState == 1){
        sound.play(offset);
      }
    }

    sound.stop = function(pause){
      if (sound && sound.source){
        sound.source && sound.source.stop();
        sound.source.disconnect()
        sound.source = undefined;
      }        
      sound.playState = 0;
      stopPlayTimer();
      wants_to_play = undefined;
      if (options.onstop && !pause){options.onstop()}
    };

    sound.togglePause = function(){
      if (sound.playState == 1) {
        sound.stop(true)
        options.onpause && options.onpause();
      } else {
        sound.play(sound.position)
        options.onresume && options.onresume();
      }
    }

    sound.destruct = function(){
      sound.source && sound.source.stop();
      sound.playState = 0;
      stopPlayTimer();
      options.onstop && options.onstop()
      for (var i=0;i<all_sounds.length;i++){
        if (all_sounds[i] === sound){
          all_sounds[i] = undefined;
          all_sounds.splice(i,1);
          break
        }
      }
      setTimeout(function(){sound = undefined},0);
    };

    //----------------------------------------------------------

    function whilePlaying(){
      if (play_tmr) {
        clearTimeout(play_tmr)
      };
      sound.position = (webManagerAudioContext.currentTime * 1000) - start_time + sound.start_offset;
      if (options.whileplaying){
        options.whileplaying()
      }
      play_tmr = setTimeout(whilePlaying,33)
    }

    function stopPlayTimer(){
        if (play_tmr) {
          clearInterval(play_tmr)
        };
        play_tmr = undefined;              
    }

    var array_buffer;
    var initSound = function(arrayBuffer) {
      array_buffer = arrayBuffer;      
      webManagerAudioContext.decodeAudioData(arrayBuffer, function (buffer) {
          audio_buffer = buffer;
          if (wants_to_play){sound.play(sound.position)}
          if (options.onload){options.onload(true)}
      }, function (e) {
          console.log('Error decoding file', e);
          if (options.onload){options.onload(false)}
      });
    }

    //----------------------------------------------------------

    initSound(sound_arrayBuffer)

    all_sounds.push(sound);

    return sound

  },

  stopAll: stopAll
}

})();        
